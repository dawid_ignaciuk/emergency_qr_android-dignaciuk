package com.thegoodhealthnetwork.emergencyqr.satatistic;

import java.io.Serializable;
import java.util.Date;

public class DayStatisticModel implements Serializable {
	private static final long serialVersionUID = 7324932129973299510L;
	
	private Date dateTime;
	private Integer our_code_created=0;
	private Integer our_code_send=0;
	private Integer our_code_saved=0;
	private Integer code_shared=0;
	private Integer our_code_read=0;
	private Integer code_read=0;
	private Integer app_started=0;
	
	public Date getDateTime() {
		return dateTime;
	}
	
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
	
	public Integer getOur_code_created() {
		return our_code_created;
	}
	
	public void setOur_code_created(Integer our_code_created) {
		this.our_code_created = our_code_created;
	}
	
	public Integer getOur_code_send() {
		return our_code_send;
	}
	
	public void setOur_code_send(Integer our_code_send) {
		this.our_code_send = our_code_send;
	}
	public Integer getOur_code_saved() {
		return our_code_saved;
	}
	
	public void setOur_code_saved(Integer our_code_saved) {
		this.our_code_saved = our_code_saved;
	}
	
	public Integer getCode_shared() {
		return code_shared;
	}
	
	public void setCode_shared(Integer code_shared) {
		this.code_shared = code_shared;
	}
	public Integer getOur_code_read() {
		return our_code_read;
	}
	public void setOur_code_read(Integer our_code_read) {
		this.our_code_read = our_code_read;
	}
	public Integer getCode_read() {
		return code_read;
	}
	public void setCode_read(Integer code_read) {
		this.code_read = code_read;
	}
	public Integer getApp_started() {
		return app_started;
	}
	public void setApp_started(Integer app_started) {
		this.app_started = app_started;
	}
	
	
}
