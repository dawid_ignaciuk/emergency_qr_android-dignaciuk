package com.thegoodhealthnetwork.emergencyqr.satatistic;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Statistics implements Serializable {
	
	private static final long serialVersionUID = 1038038464196765087L;
	
	private Map<String,DayStatisticModel> stats;
	
	
	public Statistics() {
		
	}
	
	
	public DayStatisticModel getDayStatisticModel(Date date) {
		if(stats == null) {
			stats = new HashMap<>();
		}
		
		DayStatisticModel dayStatisticModel = null;
		
		
		dayStatisticModel = stats.get(getDateString(date));
		
		if(dayStatisticModel==null) {
			dayStatisticModel = new DayStatisticModel();
			dayStatisticModel.setDateTime(date);
			stats.put(getDateString(date), dayStatisticModel);
		}
		
		return dayStatisticModel;
	}
	
	private String getDateString(Date date) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		return simpleDateFormat.format(date);
	}
	
	
	public List<DayStatisticModel> getOlder(Date date) {
		if(stats == null) {
			stats = new HashMap<>();
		}
		
		List<DayStatisticModel> older = new ArrayList<>();
		
		Set<String> keys = stats.keySet();
		
		String currentDate = getDateString(date);
		for (String key : keys) {
			if(!key.equals(currentDate)) {
				older.add(stats.get(key));
			}
		}
		
		return older;
	}

	
	public void removeOlder(Date date) {	
		if(stats == null) {
			stats = new HashMap<>();
		}
		
		Set<String> keys = stats.keySet();
		
		String currentDate = getDateString(date);
		List<String> toRemoves = new ArrayList<>();
		for (String key : keys) {
			if(!key.equals(currentDate)) {
				toRemoves.add(key);
			}
		}
		
		for (String toRemove : toRemoves) {
			stats.remove(toRemove);
		}
	}
}
