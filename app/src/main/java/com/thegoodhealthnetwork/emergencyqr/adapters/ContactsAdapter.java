package com.thegoodhealthnetwork.emergencyqr.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.List;
import com.thegoodhealthnetwork.emergencyqr.R;
import com.thegoodhealthnetwork.emergencyqr.model.ContactElement;

/**
 * Created by kamil on 19.01.2016.
 */
public class ContactsAdapter extends ArrayAdapter<ContactElement> {
    private final Context context;
    private int layoutResourceId;
    List<ContactElement> data = null;

    public ContactsAdapter(Context context,List<ContactElement> data) {
        super(context, R.layout.scanned_row,data);
        this.context = context;
        this.data = data;
        this.layoutResourceId = R.layout.scanned_row;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        ViewHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ViewHolder();
            holder.type = (TextView) row.findViewById(R.id.typeLabel);
            holder.value = (TextView) row.findViewById(R.id.valueLabel);

            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        ContactElement contactElement = data.get(position);
        holder.value.setText(contactElement.getText());

        if(contactElement.getType() == ContactElement.LINK) {
            holder.type.setText("Visit:");
        }
        else if(contactElement.getType() == ContactElement.SMS) {
            holder.type.setText("Message:");
        }
        else if(contactElement.getType() == ContactElement.PHONE) {
            holder.type.setText("Call:");
        }
        else if(contactElement.getType() == ContactElement.EMAIL) {
            holder.type.setText("E-mail:");
        }

        return row;
    }


    static class ViewHolder {
        TextView type;
        TextView value;
        int position;
    }

}

