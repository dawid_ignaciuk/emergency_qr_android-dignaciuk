package com.thegoodhealthnetwork.emergencyqr.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import com.thegoodhealthnetwork.emergencyqr.R;
import com.thegoodhealthnetwork.emergencyqr.adapters.ContactsAdapter;
import com.thegoodhealthnetwork.emergencyqr.dialogs.ShareQuestionDialog;
import com.thegoodhealthnetwork.emergencyqr.dialogs.ShareQuestionDialog.ShareQuestionListener;
import com.thegoodhealthnetwork.emergencyqr.model.ContactElement;
import com.thegoodhealthnetwork.emergencyqr.satatistic.StatisticManager;

public class QrDecodedUnknowActivity extends Activity {
	
	private static final String TAG = "QREncoderMessageActivity";
	
	private TextView decodedQrcodeValue;
	private ImageView qrCodeImage;
	private Bitmap qrCodeImageBitmap;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_qr_decoded_unknown);
		
		StatisticManager.increaseCodeRead(new Date(System.currentTimeMillis()));
		
		String decodetqrcode = getIntent().getStringExtra("decodetqrcode");
		
		decodedQrcodeValue = (TextView)findViewById(R.id.decodedQrcodeValue);
		decodedQrcodeValue.setText(decodetqrcode);
		
		decodedQrcodeValue.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if (decodedQrcodeValue.getSelectionStart() > -1 && decodedQrcodeValue.getSelectionEnd() > -1) {
					ShareQuestionDialog shareQuestionDialog = new ShareQuestionDialog(QrDecodedUnknowActivity.this);
					shareQuestionDialog.setShareQuestionListener(new ShareQuestionListenerImpl());
					shareQuestionDialog.show();
				}
			}
		});


		ListView contactsListView = (ListView)findViewById(R.id.contactsListView);
		contactsListView.setAdapter(new ContactsAdapter(this, prepareContacts(decodetqrcode)));

		int[] colors = {0,0x00CCC7C8, 0}; // red for the example
		contactsListView.setDivider(new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, colors));
		contactsListView.setDividerHeight(1);
	}

	private List<ContactElement> prepareContacts(String decodedText) {
		List<ContactElement> elements = new ArrayList<>();

		List<String> phones = getPhonesFromText(decodedText);

		for (String phone: phones) {
			elements.add(new ContactElement(ContactElement.PHONE,phone));
		}

		for (String phone: phones) {
			elements.add(new ContactElement(ContactElement.SMS,phone));
		}

		List<String> emails = getEmailsFromText(decodedText);

		String cleanedText = decodedText;
		for (String email: emails) {
			elements.add(new ContactElement(ContactElement.EMAIL,email));
			cleanedText = cleanedText.replaceAll(email,"");
		}

		List<String> webUrls = getWebUrlsFromText(cleanedText);

		for (String webUrl: webUrls) {
			elements.add(new ContactElement(ContactElement.LINK,webUrl));
		}

		return elements;
	}


	private class ShareQuestionListenerImpl implements ShareQuestionListener {

		@Override
		public void onYesClick() {
			Intent intent = new Intent("com.thegoodhealthnetwork.emergencyqr.activity.QrShareActivity");
			startActivity(intent);
		}
	}


	private List<String> getEmailsFromText(String text) {
		List<String> emails = new ArrayList<>();

		Matcher matcher = android.util.Patterns.EMAIL_ADDRESS.matcher(text);

		while(matcher.find()) {
			emails.add(matcher.group());
		}

		return emails;
	}

	private List<String> getPhonesFromText(String text) {
		List<String> phones = new ArrayList<>();

		Matcher matcher = Patterns.PHONE.matcher(text);

		while (matcher.find()) {
			phones.add(matcher.group());
		}

		return phones;
	}

	private List<String> getWebUrlsFromText(String text) {
		List<String> webUrls = new ArrayList<>();

		Matcher matcher = Patterns.WEB_URL.matcher(text);

		while(matcher.find()) {
			webUrls.add(matcher.group());
		}

		return webUrls;
	}

}
