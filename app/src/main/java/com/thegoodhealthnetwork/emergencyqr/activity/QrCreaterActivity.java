package com.thegoodhealthnetwork.emergencyqr.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import java.util.Date;
import com.thegoodhealthnetwork.emergencyqr.R;
import com.thegoodhealthnetwork.emergencyqr.dao.AppDataDao;
import com.thegoodhealthnetwork.emergencyqr.dao.QrCodeDataDao;
import com.thegoodhealthnetwork.emergencyqr.dao.SettingsDataDao;
import com.thegoodhealthnetwork.emergencyqr.dialogs.EditBoxDialog;
import com.thegoodhealthnetwork.emergencyqr.dialogs.EditBoxDialog.EditBoxDialogListener;
import com.thegoodhealthnetwork.emergencyqr.dialogs.ErrorDialog;
import com.thegoodhealthnetwork.emergencyqr.dialogs.InfoDialog1;
import com.thegoodhealthnetwork.emergencyqr.dialogs.InfoDontShowDialog;
import com.thegoodhealthnetwork.emergencyqr.dialogs.InfoDontShowDialog.InfoDontShowDialogListener;
import com.thegoodhealthnetwork.emergencyqr.dialogs.PasswordDialog;
import com.thegoodhealthnetwork.emergencyqr.dialogs.SettingsDialog;
import com.thegoodhealthnetwork.emergencyqr.dialogs.SufficientInformationDialog.SufficientInformationDialogListener;
import com.thegoodhealthnetwork.emergencyqr.model.AppDataModel;
import com.thegoodhealthnetwork.emergencyqr.model.QRCodeDataModel;
import com.thegoodhealthnetwork.emergencyqr.model.SettingsModel;
import com.thegoodhealthnetwork.emergencyqr.satatistic.StatisticManager;

public class QrCreaterActivity extends Activity {
	public static final int PICK_CONTACT = 1;
	public static final int PICK_OWNER = 2;

	private static final String TAG = "QREncoderMessageActivity";

	private EditText phoneOwnerFirstName;
	private EditText phoneOwnerLastName;
	private EditText phoneOwnerEmail;
	private EditText contactPersonFirstName;
	private EditText contactPersonLastName;
	private EditText contactPersonEmail;
	private EditText contactPersonPhone;
	private EditText contactPersonReleationship;
	private CheckBox isHealthProxy;
	private EditText medicationsField;
	private EditText conditionField;
	private EditText allergiesField;
	private EditText otherField;

	private boolean sufficientDialogShowed;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		sufficientDialogShowed = false;
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_qr_create);

		phoneOwnerFirstName = (EditText) findViewById(R.id.ownerFirstName);
		phoneOwnerFirstName.setOnFocusChangeListener(new FildFocusListener());

		phoneOwnerLastName = (EditText) findViewById(R.id.ownerLastName);
		phoneOwnerLastName.setOnFocusChangeListener(new FildFocusListener());
		
		phoneOwnerEmail = (EditText) findViewById(R.id.ownerEmail);
		phoneOwnerEmail.setOnFocusChangeListener(new FildFocusListener());

		contactPersonFirstName = (EditText) findViewById(R.id.contactPersonFirstName);
		contactPersonFirstName.setOnFocusChangeListener(new FildFocusListener());

		contactPersonLastName = (EditText) findViewById(R.id.contactPersonLastName);
		contactPersonLastName.setOnFocusChangeListener(new FildFocusListener());
		
		contactPersonPhone = (EditText) findViewById(R.id.contactPersonPhone);
		contactPersonPhone.setOnFocusChangeListener(new FildFocusListener());
		
		contactPersonEmail = (EditText) findViewById(R.id.contactPersonEmail);
		contactPersonEmail.setOnFocusChangeListener(new FildFocusListener());

		contactPersonReleationship = (EditText) findViewById(R.id.contactPersonReleationship);
		contactPersonReleationship.setOnFocusChangeListener(new FildFocusListener());
		
		isHealthProxy = (CheckBox) findViewById(R.id.isHealthProxy);
		isHealthProxy.setOnFocusChangeListener(new FildFocusListener());

		medicationsField = (EditText) findViewById(R.id.medicationsField);
		medicationsField.setOnFocusChangeListener(new FildFocusListener());
		medicationsField.setOnTouchListener(new MedicationFieldClickListener());
		
		
		conditionField = (EditText) findViewById(R.id.conditionField);
		conditionField.setOnFocusChangeListener(new FildFocusListener());
		conditionField.setOnTouchListener(new ConditionnFieldClickListener());

		allergiesField = (EditText) findViewById(R.id.allergiesField);
		allergiesField.setOnFocusChangeListener(new FildFocusListener());
		allergiesField.setOnTouchListener(new AllergiesFieldClickListener());

		otherField = (EditText) findViewById(R.id.otherField);
		otherField.setOnFocusChangeListener(new FildFocusListener());
		otherField.setOnTouchListener(new OtherFieldClickListener());

		Button previewBtn = (Button) findViewById(R.id.previewBtn);
		previewBtn.setOnClickListener(new PrevieClickListener());

		Button settingsBtn = (Button) findViewById(R.id.settingsBtn);
		settingsBtn.setOnClickListener(new SettingsClickListener());

		Button ownerAddressBookBtn = (Button) findViewById(R.id.ownerAddressBookBtn);
		ownerAddressBookBtn
				.setOnClickListener(new OwnerAddressBookButtonClickListener());

		Button contactAddressBookBtn = (Button) findViewById(R.id.contactAddressBookBtn);
		contactAddressBookBtn
				.setOnClickListener(new ContactAddressBookButtonClickListener());

		try {
			showInfoDialog();
		} catch (Exception e) {
			e.printStackTrace();
		}

		loadQrCodeData();
	}


	private void loadQrCodeData() {
		QrCodeDataDao qrCodeDataDao = new QrCodeDataDao();

		try {
			QRCodeDataModel qrCodeDataModel = qrCodeDataDao.loadQrCodeData();

			if (qrCodeDataModel.getPhonneOwnerName() != null
					&& !qrCodeDataModel.getPhonneOwnerName().isEmpty()) {

				String[] parts = qrCodeDataModel.getPhonneOwnerName().split(" ");

				if(parts.length==2) {
					phoneOwnerFirstName.setText(parts[0]);
					phoneOwnerLastName.setText(parts[1]);
				}
				else if(parts.length==1) {
					phoneOwnerFirstName.setText(parts[0]);
				}
			}


			if (qrCodeDataModel.getPhoneOwnerEmail() != null
					&& !qrCodeDataModel.getPhoneOwnerEmail().isEmpty())
				phoneOwnerEmail.setText(qrCodeDataModel.getPhoneOwnerEmail());

			if (qrCodeDataModel.getEmergencyPersonName() != null
					&& !qrCodeDataModel.getEmergencyPersonName().isEmpty()) {
				String[] parts = qrCodeDataModel.getEmergencyPersonName().split(" ");

				if(parts.length==2) {
					contactPersonFirstName.setText(parts[0]);
					contactPersonLastName.setText(parts[1]);
				}
				else if(parts.length==1) {
					contactPersonFirstName.setText(parts[0]);
				}
			}


			if (qrCodeDataModel.getEmergencyPersonPhone() != null
					&& !qrCodeDataModel.getEmergencyPersonPhone().isEmpty())
				contactPersonPhone.setText(qrCodeDataModel
						.getEmergencyPersonPhone());

			if (qrCodeDataModel.getEmergencyPersonEmail() != null
					&& !qrCodeDataModel.getEmergencyPersonEmail().isEmpty())
				contactPersonEmail.setText(qrCodeDataModel
						.getEmergencyPersonEmail());

			if (qrCodeDataModel.getEmergencyPersonRelationShip() != null
					&& !qrCodeDataModel.getEmergencyPersonRelationShip().isEmpty())
				contactPersonReleationship.setText(qrCodeDataModel
						.getEmergencyPersonRelationShip());

			isHealthProxy.setChecked(qrCodeDataModel.isHealthProxy());

			if (qrCodeDataModel.getMedications() != null
					&& !qrCodeDataModel.getMedications().isEmpty() && !qrCodeDataModel.getMedications().equals(getString(R.string.emptyValue)))
				medicationsField.setText(qrCodeDataModel.getMedications());

			if (qrCodeDataModel.getCondition() != null
					&& !qrCodeDataModel.getCondition().isEmpty() && !qrCodeDataModel.getCondition().equals(getString(R.string.emptyValue)))
				conditionField.setText(qrCodeDataModel.getCondition());

			if (qrCodeDataModel.getAllergies() != null
					&& !qrCodeDataModel.getAllergies().isEmpty() && !qrCodeDataModel.getAllergies().equals(getString(R.string.emptyValue)))
				allergiesField.setText(qrCodeDataModel.getAllergies());

			if (qrCodeDataModel.getOther() != null
					&& !qrCodeDataModel.getOther().isEmpty() && !qrCodeDataModel.getOther().equals(getString(R.string.emptyValue)))
				otherField.setText(qrCodeDataModel.getOther());

		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			errorMessageWindow(e.getMessage());
		}
	}
	
	
	private void showContactFieldsError(String errorMessage) {
		ErrorDialog errorDialog = new ErrorDialog(this, errorMessage);
		errorDialog.show();
	}

	private QRCodeDataModel save() {
		QRCodeDataModel qrCodeDataModel = new QRCodeDataModel();

		qrCodeDataModel.setPhonneOwnerName(removeNewLineChar(phoneOwnerFirstName
				.getText().toString()+" "+phoneOwnerLastName.getText().toString()));

		qrCodeDataModel.setPhoneOwnerEmail(removeNewLineChar(phoneOwnerEmail
				.getText().toString()));
		qrCodeDataModel
				.setEmergencyPersonName(removeNewLineChar(contactPersonFirstName.getText().toString()+" "+contactPersonLastName.getText().toString()));
		qrCodeDataModel
				.setEmergencyPersonEmail(removeNewLineChar(contactPersonEmail
						.getText().toString()));
		qrCodeDataModel
				.setEmergencyPersonPhone(removeNewLineChar(contactPersonPhone
						.getText().toString()));
		qrCodeDataModel.setEmergencyPersonRelationShip(removeNewLineChar(contactPersonReleationship.getText().toString()));

		qrCodeDataModel.setHealthProxy(isHealthProxy.isChecked());
		
		String medicationFieldValue = medicationsField
				.getText().toString();
		
		if(medicationFieldValue.isEmpty()) {
			medicationFieldValue = getString(R.string.emptyValue);
		}
		else {
			medicationFieldValue = removeNewLineChar(medicationFieldValue);
		}
		
		qrCodeDataModel.setMedications(medicationFieldValue);
		
		String conditionFieldValue = conditionField
				.getText().toString();
		
		if(conditionFieldValue.isEmpty()) {
			conditionFieldValue = getString(R.string.emptyValue);
		}
		else {
			conditionFieldValue = removeNewLineChar(conditionFieldValue);
		}
		
		qrCodeDataModel.setCondition(conditionFieldValue);
		
		String allergiesFieldValue = allergiesField
				.getText().toString();
		
		if(allergiesFieldValue.isEmpty()) {
			allergiesFieldValue = getString(R.string.emptyValue);
		}
		else {
			allergiesFieldValue = removeNewLineChar(allergiesFieldValue);
		}
		
		qrCodeDataModel.setAllergies(allergiesFieldValue);
		
		String otherFieldValue = otherField
				.getText().toString();
		
		if(otherFieldValue.isEmpty()) {
			otherFieldValue = getString(R.string.emptyValue);
		}
		else {
			otherFieldValue = removeNewLineChar(otherFieldValue);
		}

		qrCodeDataModel.setOther(otherFieldValue);

		QrCodeDataDao qrCodeDataDao = new QrCodeDataDao();
		try {
			qrCodeDataDao.saveQrCodeData(qrCodeDataModel);

			return qrCodeDataModel;
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			errorMessageWindow(e.getMessage());
		}
		return null;
	}
	
	
	private String checkContatctFields() {
		
		if(contactPersonFirstName.getText().toString().isEmpty() || contactPersonLastName.getText().toString().isEmpty() || contactPersonEmail.getText().toString().isEmpty() || contactPersonPhone.getText().toString().isEmpty()
				|| contactPersonReleationship.getText().toString().isEmpty()) {
			return getResources().getString(R.string.qrCodeCreateError1);
		}
		else if (phoneOwnerFirstName.getText().toString().isEmpty() || phoneOwnerLastName.getText().toString().isEmpty()) {
			return getResources().getString(R.string.qrCodeCreateError2);
		}
		else if(!phoneOwnerEmail.getText().toString().isEmpty() && !android.util.Patterns.EMAIL_ADDRESS.matcher(phoneOwnerEmail.getText().toString()).matches()) {
			return getResources().getString(R.string.qrCodeCreateWrongEmailError).replaceAll("%s",phoneOwnerEmail.getText().toString());
		}
		else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(contactPersonEmail.getText().toString()).matches()) {
			return getResources().getString(R.string.qrCodeCreateWrongEmailError).replaceAll("%s",contactPersonEmail.getText().toString());
		}
	
		return null;
	}

	private class PrevieClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {

			String errorMessage = checkContatctFields();

			if(errorMessage==null) {
				createQrCode();
			}
			else {
				sufficientDialogShowed=false;
				showContactFieldsError(errorMessage);
			}
		}
	}
	
	private void createQrCode() {
		QRCodeDataModel qrCodeDataModel = save();
		
		Intent intent = new Intent(
				"com.thegoodhealthnetwork.emergencyqr.activity.QrCreatedActivity");
		intent.putExtra("qrcodedata", qrCodeDataModel);
		startActivity(intent);

		StatisticManager.increaseOurCodeCreated(new Date(System
				.currentTimeMillis()));
	}

	private void showSettingsDialog(boolean showCancelButton) {
		SettingsDialog settingsDialog = new SettingsDialog(
				QrCreaterActivity.this, showCancelButton);
		settingsDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {
			}
		});
		settingsDialog.show();
	}

	private class SettingsClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {

			SettingsModel settingsModel = getSettingsModel();

			if(settingsModel==null) {
				showSettingsDialog(true);
			}
			else if(settingsModel!=null && settingsModel.isPasswordDisabled()) {
				showSettingsDialog(true);
			}
			else {
				PasswordDialog passwordDialog = new PasswordDialog(QrCreaterActivity.this);
				passwordDialog.setLoginListener(new PasswordDialog.LoginListener() {
					@Override
					public void onLogin() {
						showSettingsDialog(true);
					}
				});
				passwordDialog.show();
			}
		}
	}

	private void errorMessageWindow(String message) {
		AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
		dlgAlert.setMessage(message);
		dlgAlert.show();
	}

	private class MedicationFieldClickListener implements OnTouchListener {

		@Override
		public boolean onTouch(View v, MotionEvent event) {

			if (event.getAction() == MotionEvent.ACTION_DOWN) {

				EditBoxDialog editBoxDialog = new EditBoxDialog(
						QrCreaterActivity.this, "Medications", medicationsField
								.getText().toString());
				editBoxDialog
						.setEditBoxDialogListener(new EditBoxDialogListener() {

							@Override
							public void onOK(String value) {
								medicationsField
										.setText(removeNewLineChar(value));

							}
						});

				editBoxDialog.show();
			}
			return true;
		}

	}

	private class ConditionnFieldClickListener implements OnTouchListener {

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				EditBoxDialog editBoxDialog = new EditBoxDialog(
						QrCreaterActivity.this, "Condition", conditionField
								.getText().toString());
				editBoxDialog
						.setEditBoxDialogListener(new EditBoxDialogListener() {

							@Override
							public void onOK(String value) {
								conditionField
										.setText(removeNewLineChar(value));

							}
						});
				editBoxDialog.show();
			}
			return true;
		}
	}

	private class AllergiesFieldClickListener implements OnTouchListener {

		@Override
		public boolean onTouch(View v, MotionEvent event) {

			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				EditBoxDialog editBoxDialog = new EditBoxDialog(
						QrCreaterActivity.this, "Allergies", allergiesField
								.getText().toString());
				editBoxDialog
						.setEditBoxDialogListener(new EditBoxDialogListener() {

							@Override
							public void onOK(String value) {
								allergiesField
										.setText(removeNewLineChar(value));

							}
						});
				editBoxDialog.show();
			}
			return true;
		}
	}

	private class OtherFieldClickListener implements OnTouchListener {

		@Override
		public boolean onTouch(View v, MotionEvent event) {

			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				EditBoxDialog editBoxDialog = new EditBoxDialog(
						QrCreaterActivity.this, "Notes", otherField.getText()
								.toString());
				editBoxDialog
						.setEditBoxDialogListener(new EditBoxDialogListener() {

							@Override
							public void onOK(String value) {
								otherField.setText(removeNewLineChar(value));

							}
						});
				editBoxDialog.show();
			}

			return true;
		}
	}

	private String removeNewLineChar(String value) {
		return value.replaceAll("\n", ", ").replaceAll(":", " ");
	}

	private class OwnerAddressBookButtonClickListener implements
			OnClickListener {

		@Override
		public void onClick(View v) {
			Intent intent = new Intent(Intent.ACTION_PICK);
			intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
			startActivityForResult(intent, PICK_OWNER);

		}

	}

	private class ContactAddressBookButtonClickListener implements
			OnClickListener {

		@Override
		public void onClick(View v) {
			Intent intent = new Intent(Intent.ACTION_PICK);
			intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
			startActivityForResult(intent, PICK_CONTACT);

		}

	}

	@Override
	public void onActivityResult(int reqCode, int resultCode, Intent data) {
		super.onActivityResult(reqCode, resultCode, data);

		switch (reqCode) {
			case (PICK_CONTACT):
				if (resultCode == Activity.RESULT_OK) {
					getContactDataFromAddressBook(data);
				}
				break;
			case (PICK_OWNER):
				if (resultCode == Activity.RESULT_OK) {
					getOwnerDataFromAddressBook(data);
				}
				break;
		}
	}

	private void getContactDataFromAddressBook(Intent data) {

		Uri contactData = data.getData();
		Cursor cursor = getContentResolver().query(contactData, null, null,
				null, null);
		if (cursor.moveToFirst()) {
			String contactId = cursor.getString(cursor
					.getColumnIndex(ContactsContract.Contacts._ID));

			// String dsiplayName =
			// cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME));
			String dsiplayName = cursor.getString(cursor
					.getColumnIndex("display_name"));

			if(dsiplayName!=null && !dsiplayName.isEmpty()) {
				String[] parts = dsiplayName.split(" ");

				if(parts.length>=2) {
					contactPersonFirstName.setText(parts[0]);
					contactPersonLastName.setText(parts[1]);
				}
				else if(parts.length==1) {
					contactPersonFirstName.setText(parts[0]);
				}
			}

			String hasPhone = cursor
					.getString(cursor
							.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

			if (hasPhone.equalsIgnoreCase("1"))
				hasPhone = "true";
			else
				hasPhone = "false";

			if (Boolean.parseBoolean(hasPhone)) {
				Cursor phones = getContentResolver().query(
						ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
						null,
						ContactsContract.CommonDataKinds.Phone.CONTACT_ID
								+ " = " + contactId, null, null);
				while (phones.moveToNext()) {
					String phoneNumber = phones
							.getString(phones
									.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

					contactPersonPhone.setText(phoneNumber);

				}
				phones.close();
			}

			Cursor emails = getContentResolver().query(
					ContactsContract.CommonDataKinds.Email.CONTENT_URI,
					null,
					ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = "
							+ contactId, null, null);
			while (emails.moveToNext()) {
				String emailAddress = emails
						.getString(emails
								.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
				contactPersonEmail.setText(emailAddress);

			}
			emails.close();
		}
		cursor.close();
	}

	private void getOwnerDataFromAddressBook(Intent data) {

		Uri contactData = data.getData();
		Cursor cursor = getContentResolver().query(contactData, null, null,
				null, null);
		if (cursor.moveToFirst()) {
			String contactId = cursor.getString(cursor
					.getColumnIndex(ContactsContract.Contacts._ID));

			// String dsiplayName =
			// cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME));
			String dsiplayName = cursor.getString(cursor
					.getColumnIndex("display_name"));

			if(dsiplayName!=null && !dsiplayName.isEmpty()) {
				String[] parts = dsiplayName.split(" ");

				if(parts.length>=2) {
					phoneOwnerFirstName.setText(parts[0]);
					phoneOwnerLastName.setText(parts[1]);
				}
				else if(parts.length==1) {
					phoneOwnerFirstName.setText(parts[0]);
				}
			}

			Cursor emails = getContentResolver().query(
					ContactsContract.CommonDataKinds.Email.CONTENT_URI,
					null,
					ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = "
							+ contactId, null, null);
			while (emails.moveToNext()) {
				String emailAddress = emails
						.getString(emails
								.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
				phoneOwnerEmail.setText(emailAddress);

			}
			emails.close();
		}
		cursor.close();
	}
	
	private boolean showInfoDialog() throws Exception {
		AppDataDao appDataDao = new AppDataDao();
		
		AppDataModel appDataModel = appDataDao.load();
		
		if(appDataModel==null || !appDataModel.isDontShowCreateMessage1()) {


			InfoDialog1 infoDialog1 = new InfoDialog1(QrCreaterActivity.this);
			infoDialog1.setInfoTitle((String) QrCreaterActivity.this.getText(R.string.howToCreateQrCodeTitle));
			infoDialog1.setInfoText((String) QrCreaterActivity.this.getText(R.string.howToCreateQrCodeMessage1));
			infoDialog1.setInfoDialog1Listener(new InfoDialog1.InfoDialog1Listener() {
				@Override
				public void nextClick() {

					InfoDontShowDialog infoDontShowDialog = new InfoDontShowDialog(QrCreaterActivity.this);
					infoDontShowDialog.setInfoTitle((String) QrCreaterActivity.this.getText(R.string.howToCreateQrCodeTitle));
					infoDontShowDialog.setInfoText((String) QrCreaterActivity.this.getText(R.string.howToCreateQrCodeMessage2));
					infoDontShowDialog.setInfoDontShowDialogListener(new InfoDontShowDialogListenerImpl());
					infoDontShowDialog.show();
				}
			});
			infoDialog1.show();

			return true;
		}
		return false;
	}
	
	
	private class InfoDontShowDialogListenerImpl implements InfoDontShowDialogListener {

		@Override
		public void onDontShowClick() {
			saveMessageCounter(true);
			
		}

		@Override
		public void onOkClick() {
			saveMessageCounter(false);
			
		}
		
	}

	
	private void saveMessageCounter(boolean disableMessage) {
		AppDataDao appDataDao = new AppDataDao();
		
		AppDataModel appDataModel;
		try {
			appDataModel = appDataDao.load();

			if(appDataModel==null) {
				appDataModel = new AppDataModel();
			}
			
			appDataModel.setDontShowCreateMessage1(disableMessage);
			appDataModel.setCreateMessage1NuberOfShow(appDataModel.getCreateMessage1NuberOfShow()+1);
			
			appDataDao.save(appDataModel);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private class FildFocusListener implements OnFocusChangeListener {

		@Override
		public void onFocusChange(View arg0, boolean arg1) {
			/*if(!sufficientDialogShowed && checkContatctFields()) {
				*//*SufficientInformationDialog sufficientInformationDialog = new SufficientInformationDialog(QrCreaterActivity.this);
				sufficientInformationDialog.setSufficientInformationDialogListener(new SufficientInformationDialogListenerImpl());
				sufficientInformationDialog.show();*//*
			}*/
			
		}
		
	}
	
	private class SufficientInformationDialogListenerImpl implements SufficientInformationDialogListener {

		@Override
		public void onCreateClick() {
			sufficientDialogShowed=true;
			createQrCode();
		}

		@Override
		public void onContinueClick() {
			sufficientDialogShowed=true;
		}
		
	}

	private SettingsModel getSettingsModel() {
		SettingsDataDao settingsDataDao = new SettingsDataDao();
		try {
			return settingsDataDao.load();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
