package com.thegoodhealthnetwork.emergencyqr.activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;
import com.thegoodhealthnetwork.emergencyqr.R;

public class ComposerPreview extends Activity {

    public static final int PREVIEW_ACTIVITY_CANCELED =-300;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.composer_preview);

        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        String filename =  getIntent().getStringExtra("filename");
        Bitmap composerbitmap = BitmapFactory.decodeFile(filename);
        imageView.setImageBitmap(composerbitmap);
    }


    @Override
    public void onBackPressed() {
        setResult(PREVIEW_ACTIVITY_CANCELED);
        finish();
    }
}
