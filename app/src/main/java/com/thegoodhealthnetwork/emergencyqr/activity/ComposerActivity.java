package com.thegoodhealthnetwork.emergencyqr.activity;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import com.thegoodhealthnetwork.emergencyqr.R;
import com.thegoodhealthnetwork.emergencyqr.dialogs.ComposerDialog;
import com.thegoodhealthnetwork.emergencyqr.dialogs.ComposerImageDialog;
import com.thegoodhealthnetwork.emergencyqr.dialogs.SetAsLockScreenDialog;
import com.thegoodhealthnetwork.emergencyqr.dialogs.ShareQuestionDialog;
import com.thegoodhealthnetwork.emergencyqr.generator.QrCodeImageGenerator;
import com.thegoodhealthnetwork.emergencyqr.global.ComposerDataGLobal;
import com.thegoodhealthnetwork.emergencyqr.model.QRCodeDataModel;
import com.thegoodhealthnetwork.emergencyqr.satatistic.StatisticManager;
import com.thegoodhealthnetwork.emergencyqr.views.OtizTouchImageView;

public class ComposerActivity extends Activity {
    public static final int COMPOSER_ACTIVITY_CANCELED =-200;
    static final int REQUEST_CAMERA_IMAGE_CAPTURE = 1;
    static final int REQUEST_GALERY_IMAGE_CAPTURE = 2;

    private static final int START_COMPOSER_PREVIEW_ACTIVITY = 300;

    private ImageView currentImageView;
    private ImageView imageView4;
    private LinearLayout linearLayout;

    private OtizTouchImageView imageView1;
    private OtizTouchImageView imageView2;
    private OtizTouchImageView imageView3;
    private OtizTouchImageView imageView5;
    private OtizTouchImageView imageView6;
    private Uri imageUri;
    private QRCodeDataModel qrcodedata;

    private static final int START_SHARE_ACTIVITY = 400;
    private static final int START_SETTINGS_ACTIVITY = 800;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_composer);

        qrcodedata = (QRCodeDataModel)getIntent().getSerializableExtra("qrcodedata");
        generateQrCode(qrcodedata);

        imageView1 = (OtizTouchImageView) findViewById(R.id.imageView1);

        imageView2 = (OtizTouchImageView) findViewById(R.id.imageView2);

        imageView3 = (OtizTouchImageView) findViewById(R.id.imageView3);

        imageView5 = (OtizTouchImageView) findViewById(R.id.imageView5);

        imageView6 = (OtizTouchImageView) findViewById(R.id.imageView6);

        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);

        Button actionsBtn = (Button) findViewById(R.id.actionsBtn);
        actionsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showActionDialog();
            }
        });
    }

    private void generateQrCode(QRCodeDataModel qrcodedata) {
        imageView4 = (ImageView) findViewById(R.id.imageView4);

        QrCodeImageGenerator qrCodeImageGenerator = new QrCodeImageGenerator(this);
        Bitmap qrCodeImageBitmap = qrCodeImageGenerator.generateCode(qrcodedata.toString(),false);

        imageView4.setImageBitmap(qrCodeImageBitmap);
    }


    private void initImageView(ImageView imageView) {
        imageView.setOnClickListener(new ImageViewClickListener(imageView));

        File imageFile = getCacheImageFilePath(imageView);

        if(imageFile.exists()) {
            imageView.setImageBitmap(decodeSampledBitmapFromResource(imageFile, xDim, yDim));
        }
        else {
            clearGlobal(imageView);
        }
    }

    private class ImageViewClickListener implements View.OnClickListener {
        private ImageView imageView;

        public ImageViewClickListener(ImageView imageView) {
            this.imageView = imageView;
        }

        @Override
        public void onClick(View v) {

            ComposerImageDialog composerImageDialog = new ComposerImageDialog(ComposerActivity.this,getCacheImageFilePath(imageView).exists());
            composerImageDialog.setComposerImageDialogListener(new ComposerImageDialog.ComposerImageDialogListener() {
                @Override
                public void onAddFromCameraClick() {
                    currentImageView = imageView;
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

                        clearGlobal(imageView);

                        File imageFIle = getImageFilePath(imageView);

                        if (imageFIle.exists()) {
                            imageFIle.delete();
                        }

                        Uri uri = Uri.fromFile(imageFIle);
                        imageUri = uri;

                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                        startActivityForResult(takePictureIntent, REQUEST_CAMERA_IMAGE_CAPTURE);
                    }
                }

                @Override
                public void onAddFromPhotosClick() {
                    currentImageView = imageView;

                    clearGlobal(imageView);

                    Intent takePictureIntent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    takePictureIntent.setType("image/*");

                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

                        startActivityForResult(Intent.createChooser(takePictureIntent, "Select File"), REQUEST_GALERY_IMAGE_CAPTURE);
                    }
                }

                @Override
                public void onDeleteImageClick() {
                    currentImageView = imageView;

                    File imageFile = getCacheImageFilePath(imageView);

                    if(imageFile.exists()) {
                        imageFile.delete();
                    }

                    clearGlobal(imageView);

                    Drawable myDrawable;
                    if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        myDrawable = getResources().getDrawable(R.drawable.white_rectangle, getTheme());
                    } else {
                        myDrawable = getDrawable(R.drawable.white_rectangle);
                    }

                    currentImageView.setImageDrawable(myDrawable);
                }

                @Override
                public void onCancelClick() {

                }
            });

            composerImageDialog.show();
        }
    }

    private File getCacheFile(ImageView imageView) {
        String fileName = getResources().getResourceEntryName(imageView.getId())+".png";
        return new File(getCacheDir(),fileName);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {

        if(requestCode == START_COMPOSER_PREVIEW_ACTIVITY && resultCode == ComposerPreview.PREVIEW_ACTIVITY_CANCELED){
            showActionDialog();
        }
        else if (requestCode == REQUEST_CAMERA_IMAGE_CAPTURE && resultCode == RESULT_OK) {

            if(currentImageView!=null) {

                File imageFIle = getImageFilePath(currentImageView);

                try {
                    Bitmap smalImage = decodeSampledBitmap(Uri.fromFile(imageFIle));
                    currentImageView.setImageBitmap(smalImage);

                    smalImage.compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(getCacheFile(currentImageView)));

                    imageFIle.delete();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        else if (requestCode == REQUEST_GALERY_IMAGE_CAPTURE && resultCode == RESULT_OK) {

            Uri selectedImageUri = data.getData();

            File imageFIle = getCacheImageFilePath(currentImageView);

            String pathToSelectedFile = getPath(selectedImageUri);
            File selectedFile = new File(pathToSelectedFile);

            Bitmap bitmap = decodeSampledBitmapFromResource(selectedFile, xDim, yDim);

            Bitmap rotatedBitmap = checkImageOrientation(selectedFile, bitmap);

            if(rotatedBitmap !=null) {
                bitmap = rotatedBitmap;
            }

            currentImageView.setImageBitmap(bitmap);

            if( imageFIle.exists()) {
                imageFIle.delete();
            }

            saveImageToFile(imageFIle, bitmap);
        }
    }

    private Bitmap checkImageOrientation(File orginalFile,Bitmap orginalBitmap) {
        Bitmap rotatedBitmap;

        try {
            ExifInterface exif = new ExifInterface(orginalFile.getPath());
            int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            int rotationInDegrees = exifToDegrees(rotation);
            Matrix matrix = new Matrix();
            if (rotation != 0f) {
                matrix.preRotate(rotationInDegrees);
            }

            rotatedBitmap = Bitmap.createBitmap(orginalBitmap,0,0, orginalBitmap.getWidth(), orginalBitmap.getHeight(), matrix, true);

            return rotatedBitmap;
        }catch(IOException ex){
            Log.e("EmergencyQr", "Failed to get Exif data", ex);
        }
        return null;
    }


    private int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) { return 90; }
        else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {  return 180; }
        else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {  return 270; }
        return 0;
    }

   private void saveImageToFile(File file,Bitmap imageBitmap) {
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(file);
            imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private String saveComposeImageGalery(Bitmap imageBitmap) throws IOException {

        SimpleDateFormat sdf = new SimpleDateFormat("d-M-y h:m:s");

        String fileName = "composer-emergency-qr"+sdf.format(new Date(System.currentTimeMillis()))+".png";

        /*File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);*/

        String storageDir = Environment.getExternalStoragePublicDirectory(getResources().getString(R.string.app_folder_name)).getAbsolutePath();

        // String fileFolderName = getResources().getString(R.string.app_folder_name);

        File imageFolder = new File(storageDir);

        if(!imageFolder.exists()) {
            imageFolder.mkdir();
        }


        File imageFile = new File(imageFolder,fileName);

        FileOutputStream out = null;

        out = new FileOutputStream(imageFile);
        boolean status = imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance

        out.close();

        return imageFile.getAbsolutePath();

    }

   /* private File getImageFilePath(View imageView) {
        String fileName = getResources().getResourceEntryName(imageView.getId())+".png";

        String fileFolderName = getResources().getString(R.string.app_folder_name);

        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);

        File imageFolder = new File(storageDir,fileFolderName);

        if(!imageFolder.exists()) {
            imageFolder.mkdir();
        }

        File imageFile = new File(imageFolder,fileName);

        String absolutePath = imageFile.getAbsolutePath();

        return imageFile;
    }*/

    private File getCacheImageFilePath(View imageView) {
        String fileName = getResources().getResourceEntryName(imageView.getId())+".png";
        return new File(getCacheDir(),fileName);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }


    @Override
    public void onStop() {
        super.onStop();
        ComposerDataGLobal.setImage1(imageView1.onSaveInstanceState());
        ComposerDataGLobal.setImage2(imageView2.onSaveInstanceState());
        ComposerDataGLobal.setImage3(imageView3.onSaveInstanceState());
        ComposerDataGLobal.setImage5(imageView5.onSaveInstanceState());
        ComposerDataGLobal.setImage6(imageView6.onSaveInstanceState());
    }

    @Override
    public void onStart() {
        super.onStart();

        if(ComposerDataGLobal.getImage1()!=null) {
            imageView1.onRestoreInstanceState(ComposerDataGLobal.getImage1());
        }

        if(ComposerDataGLobal.getImage2()!=null) {
            imageView2.onRestoreInstanceState(ComposerDataGLobal.getImage2());
        }

        if(ComposerDataGLobal.getImage3()!=null) {
            imageView3.onRestoreInstanceState(ComposerDataGLobal.getImage3());
        }

        if(ComposerDataGLobal.getImage5()!=null) {
            imageView5.onRestoreInstanceState(ComposerDataGLobal.getImage5());
        }

        if(ComposerDataGLobal.getImage6()!=null) {
            imageView6.onRestoreInstanceState(ComposerDataGLobal.getImage6());
        }
    }

    private void clearGlobal(ImageView imageView) {
        if(imageView==imageView1) {
            ComposerDataGLobal.setImage1(null);
            System.out.println("Wyczyszczono 1");
        }
        else if(imageView==imageView2) {
            ComposerDataGLobal.setImage2(null);
            System.out.println("Wyczyszczono 2");
        }
        else if(imageView==imageView3) {
            ComposerDataGLobal.setImage3(null);
            System.out.println("Wyczyszczono 3");
        }
        else if(imageView==imageView5) {
            ComposerDataGLobal.setImage5(null);
            System.out.println("Wyczyszczono 5");
        }
        else if(imageView==imageView6) {
            ComposerDataGLobal.setImage6(null);
            System.out.println("Wyczyszczono 6");
        }
    }


    int xDim, yDim; //stores ImageView dimensions
    int xDim4, yDim4; //stores ImageViev4 dimensions

    int screenWidth;
    int screenHeight;

    @Override
    //Get the size of the Image view after the
    //Activity has completely loaded
    public void onWindowFocusChanged(boolean hasFocus){
        super.onWindowFocusChanged(hasFocus);

        ///Display display = getWindowManager().getDefaultDisplay();

        //screenWidth = display.getWidth();
      //  screenHeight = display.getHeight();

      //  LinearLayout imageSubContainer = (LinearLayout)findViewById(R.id.imageSubContainer);

        //LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(screenWidth, screenHeight);
        //sv.setLayoutParams(layoutParams);

       // LinearLayout.LayoutParams params= new LinearLayout.LayoutParams(screenWidth, screenHeight);
       // imageSubContainer.setLayoutParams(params);
      //  imageSubContainer.refreshDrawableState();


        xDim=imageView1.getWidth();
        yDim=imageView1.getHeight();

        xDim4 = imageView4.getWidth();
        yDim4 = imageView4.getHeight();

        initImageView(imageView1);
        initImageView(imageView2);
        initImageView(imageView3);
        initImageView(imageView5);
        initImageView(imageView6);
    }

    //Load a bitmap from a resource with a target size
    static Bitmap decodeSampledBitmapFromResource(File imageFile, int reqWidth, int reqHeight) {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imageFile.getAbsolutePath(), options);
        // Calculate inSampleSize
        options.inSampleSize = 2;//calculateInSampleSize(options, reqWidth, reqHeight);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(imageFile.getAbsolutePath(),options);
    }

    //Given the bitmap size and View size calculate a subsampling size (powers of 2)
    static int calculateInSampleSize( BitmapFactory.Options options, int reqWidth, int reqHeight) {
        int inSampleSize = 1;	//Default subsampling size
        // See if image raw height and width is bigger than that of required view
        if (options.outHeight > reqHeight || options.outWidth > reqWidth) {
            //bigger
            final int halfHeight = options.outHeight / 2;
            final int halfWidth = options.outWidth / 2;
            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }


    private void mergeAllImages() {

        LinearLayout imageContaine = (LinearLayout)findViewById(R.id.imageContainer);

        Bitmap bitmap = Bitmap.createBitmap(imageContaine.getWidth(),imageContaine.getHeight(), Bitmap.Config.ARGB_4444);
        bitmap.eraseColor(getResources().getColor(R.color.green_qrcode));

        Canvas canvas = new Canvas(bitmap);

        addImageToCanvas(imageView1,canvas,1,1,2,2);
        addImageToCanvas(imageView2,canvas,1,2,6,2);
        addImageToCanvas(imageView3, canvas, 2, 1, 2, 6);
        addLinearLayoutToCanvas(linearLayout,canvas);
        addImageToCanvas(imageView5, canvas, 3, 1, 2, 12);
        addImageToCanvas(imageView6, canvas,3,2,6,12);


        Bitmap scaled = Bitmap.createScaledBitmap(bitmap,imageContaine.getWidth(),imageContaine.getHeight(),true);

        saveImageToFile(getCacheImageFilePath(imageContaine),scaled);
    }

    private void showShareQuestion() {
        ShareQuestionDialog shareQuestionDialog = new ShareQuestionDialog(ComposerActivity.this);
        shareQuestionDialog.setShareQuestionListener(new ShareQuestionListenerImpl());
        shareQuestionDialog.show();
    }

    private class ShareQuestionListenerImpl implements ShareQuestionDialog.ShareQuestionListener {

        @Override
        public void onYesClick() {
            startShareActivity();
        }
    }

    private void startShareActivity() {
        Intent intent = new Intent("com.thegoodhealthnetwork.emergencyqr.activity.QrShareActivity");
        startActivity(intent);
    }

    private void addLinearLayoutToCanvas(LinearLayout linearLayout, Canvas canv) {

        linearLayout.setDrawingCacheEnabled(true);
        linearLayout.buildDrawingCache(true);
        Bitmap bmap = linearLayout.getDrawingCache(true);

        canv.drawBitmap(bmap, (linearLayout.getWidth() * (2 - 1)) + (convertDpToPixel(6)), (linearLayout.getHeight() * (2 - 1)) + (convertDpToPixel(6)), null);
        linearLayout.setDrawingCacheEnabled(false);


    }

    private void addImageToCanvas(View imageView,Canvas canv,int row,int col,int leftMargin,int topMargin) {

        File imageFile = getCacheImageFilePath(imageView);

        if(imageFile.exists()) {

            imageView.setDrawingCacheEnabled(true);
            imageView.buildDrawingCache(true);
            Bitmap bmap = imageView.getDrawingCache(true);
            Bitmap scaled = Bitmap.createScaledBitmap(bmap, xDim, yDim, true);

            canv.drawBitmap(scaled,(imageView.getWidth()*(col-1))+(convertDpToPixel(leftMargin)),(imageView.getHeight()*(row-1))+(convertDpToPixel(topMargin)),null);
            imageView.setDrawingCacheEnabled(false);
        }

    }

    private float convertDpToPixel(float dp){
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        return dp * (metrics.densityDpi / 160f);
    }

    private class ComposerDialogListenerImpl implements ComposerDialog.ComposerDialogListener {

        @Override
        public void saveToPhotosClick() {
            mergeAllImages();

            LinearLayout imageContaine = (LinearLayout)findViewById(R.id.imageContainer);
            File mergedFile = getCacheImageFilePath(imageContaine);
            Bitmap composerBitmap = BitmapFactory.decodeFile(mergedFile.getAbsolutePath());

            try {
                String pathToComposerFile = saveComposeImageGalery(composerBitmap);
                mergedFile.delete();

                ContentValues values = new ContentValues();

                values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
                values.put(MediaStore.Images.Media.MIME_TYPE, "image/png");
                values.put(MediaStore.MediaColumns.DATA, pathToComposerFile);

                getApplicationContext().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

                Toast.makeText(ComposerActivity.this, "Composer image saved to gallery as qrcode_composer.png", Toast.LENGTH_LONG).show();

            } catch (IOException e) {
                Toast.makeText(ComposerActivity.this, "Error. Can;t save composer image to gallery", Toast.LENGTH_LONG).show();
            }
            //showShareQuestion();

            SetAsLockScreenDialog setAsLockScreenDialog = new SetAsLockScreenDialog(ComposerActivity.this);

            setAsLockScreenDialog.setSetAsLockScreenDialogListener(new SetAsLockScreenDialog.SetAsLockScreenDialogListener() {
                @Override
                public void onSetLoockScreenClick() {
                    Intent intent = new Intent(Settings.ACTION_SETTINGS);
                    startActivityForResult(intent,START_SETTINGS_ACTIVITY);
                }

                @Override
                public void onShareAppClick() {
                    Intent intent = new Intent("com.thegoodhealthnetwork.emergencyqr.activity.QrShareActivity");
                    startActivityForResult(intent,START_SHARE_ACTIVITY);
                }
            });

            setAsLockScreenDialog.show();
        }

        @Override
        public void sendByEmailClick() {
            mergeAllImages();

            LinearLayout imageContaine = (LinearLayout)findViewById(R.id.imageContainer);
            File mergedFile = getCacheImageFilePath(imageContaine);
            Bitmap composerBitmap = BitmapFactory.decodeFile(mergedFile.getAbsolutePath());

            StatisticManager.increaseOurCodeSend(new Date(System.currentTimeMillis()));

            try {

                String pathToComposerFile = saveComposeImageGalery(composerBitmap);
                mergedFile.delete();

                ArrayList<Uri> imagesUris = new ArrayList<>();
                String pathToImage = mergedFile.getAbsolutePath();
                File imageFile = new File(pathToImage);
                Uri uri = Uri.fromFile(new File(pathToComposerFile));
                imagesUris.add(uri);

                final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND_MULTIPLE);
                emailIntent.putExtra(Intent.EXTRA_STREAM,imagesUris);
                emailIntent.setType("image/*");
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getResources().getText(R.string.qrCodeEmailSubject));
                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, createEmailContent(getResources().getString(R.string.qrCodeEmailBody)));
                startActivityForResult(Intent.createChooser(emailIntent, "Send email by ..."), 3);
            } catch (IOException e) {
                Toast.makeText(ComposerActivity.this, "Error. Can't send composer image", Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void previewClick() {
            mergeAllImages();

            LinearLayout imageContainer = (LinearLayout)findViewById(R.id.imageContainer);
            File mergedFile = getCacheImageFilePath(imageContainer);

            Intent intent = new Intent("com.thegoodhealthnetwork.emergencyqr.activity.ComposerPreview");
            intent.putExtra("filename", mergedFile.getAbsolutePath());
            startActivityForResult(intent,START_COMPOSER_PREVIEW_ACTIVITY);
        }

        @Override
        public void onCancelClick() {
        }
    }


    /**
     * helper to retrieve the path of an image URI
     */
    public String getPath(Uri uri) {
        // just some safety built in
        if( uri == null ) {
            // TODO perform some logging or show user feedback
            return null;
        }
        // try to retrieve the image from the media store first
        // this will only work for images selected from gallery
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if( cursor != null ){
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            String result = cursor.getString(column_index);
            cursor.close();

            return result;
        }
        // this is our fallback here
        return uri.getPath();
    }

    private String createEmailContent(String emailText) {
        StringBuilder sb = new StringBuilder();

        sb.append(emailText);
        sb.append("\n\n");
        sb.append("It contains the following information:\n\n");

        sb.append("Phone owner\n");
        sb.append("Name: ").append(qrcodedata.getPhonneOwnerName()).append("\n");
        sb.append("E-mail: ").append(qrcodedata.getPhoneOwnerEmail()).append("\n");
        sb.append("Emergency contact\n");
        sb.append("Name: ").append(qrcodedata.getEmergencyPersonName()).append("\n");
        sb.append("Phone: ").append(qrcodedata.getEmergencyPersonPhone()).append("\n");
        sb.append("E-mail: ").append(qrcodedata.getEmergencyPersonEmail()).append("\n");
        sb.append("Relationship: ").append(qrcodedata.getEmergencyPersonRelationShip()).append("\n");
        sb.append("Health care proxy: ");

        if(qrcodedata.isHealthProxy()) {
            sb.append("YES\n");
        }
        else {
            sb.append("NO\n");
        }

        sb.append("Health information\n");

        sb.append("Medications: ");
        if(qrcodedata.getMedications()!=null && !qrcodedata.getMedications().isEmpty()) {
            sb.append(qrcodedata.getMedications()).append("\n");
        }
        else {
            sb.append("none indicated\n");
        }

        sb.append("Conditions: ");
        if(qrcodedata.getCondition()!=null && !qrcodedata.getCondition().isEmpty()) {
            sb.append(qrcodedata.getCondition()).append("\n");
        }
        else {
            sb.append("none indicated\n");
        }

        sb.append("Allergies: ");
        if(qrcodedata.getAllergies()!=null && !qrcodedata.getAllergies().isEmpty()) {
            sb.append(qrcodedata.getAllergies()).append("\n");
        }
        else {
            sb.append("none indicated\n");
        }

        sb.append("Notes: ");
        if(qrcodedata.getOther()!=null && !qrcodedata.getOther().isEmpty()) {
            sb.append(qrcodedata.getOther()).append("\n");
        }
        else {
            sb.append("none indicated\n");
        }

        return sb.toString();
    }


    private Bitmap rotateImageIfRequired(Bitmap img, Uri selectedImage) {

        // Detect rotation
        int rotation= getRotation(selectedImage);
        if(rotation!=0){
            Matrix matrix = new Matrix();
            matrix.postRotate(rotation);
            Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
            img.recycle();
            return rotatedImg;
        }else{
            return img;
        }
    }


    private int getRotation(Uri selectedImage) {
        int rotation =0;
        ContentResolver content = this.getContentResolver();

        Cursor mediaCursor = content.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[] { "orientation", "date_added" },null, null,"date_added desc");

        if (mediaCursor != null && mediaCursor.getCount() !=0 ) {
            mediaCursor.moveToNext();
            rotation = mediaCursor.getInt(0);
            mediaCursor.close();
        }
        return rotation;
    }


    private final int MAX_HEIGHT = 1024;
    private final int MAX_WIDTH = 1024;
    public Bitmap decodeSampledBitmap(Uri selectedImage)
            throws IOException {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        InputStream imageStream = getContentResolver().openInputStream(selectedImage);

        if(imageStream!=null){
            BitmapFactory.decodeStream(imageStream, null, options);
            imageStream.close();
        }

        // Calculate inSampleSize
        options.inSampleSize = 2; //calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        imageStream = getContentResolver().openInputStream(selectedImage);
        Bitmap img = BitmapFactory.decodeStream(imageStream, null, options);

        img= rotateImageIfRequired(img, selectedImage);
        return img;
    }

    @Override
    public void onBackPressed() {
        setResult(COMPOSER_ACTIVITY_CANCELED);
        finish();
    }


    private void showActionDialog() {
        ComposerDialog composerDialog = new ComposerDialog(ComposerActivity.this);
        composerDialog.setComposerDialogListener(new ComposerDialogListenerImpl());
        composerDialog.show();
    }

    private File getImageFilePath(View imageView) {
        String fileName = getResources().getResourceEntryName(imageView.getId())+".jpeg";

        String fileFolderName = getResources().getString(R.string.app_folder_name);

        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);

        File imageFolder = new File(storageDir,fileFolderName);

        if(!imageFolder.exists()) {
            imageFolder.mkdir();
        }

        File imageFile = new File(imageFolder,fileName);

        String absolutePath = imageFile.getAbsolutePath();

        return imageFile;

    }

}
