package com.thegoodhealthnetwork.emergencyqr.activity;

import com.thegoodhealthnetwork.emergencyqr.R;
import com.thegoodhealthnetwork.emergencyqr.dao.SubscribeInformationDao;
import com.thegoodhealthnetwork.emergencyqr.dialogs.CountriesDialog;
import com.thegoodhealthnetwork.emergencyqr.dialogs.CountriesDialog.CountriesDialogListener;
import com.thegoodhealthnetwork.emergencyqr.dialogs.ErrorDialog;
import com.thegoodhealthnetwork.emergencyqr.dialogs.InfoDialog;
import com.thegoodhealthnetwork.emergencyqr.dialogs.InfoDialog1;
import com.thegoodhealthnetwork.emergencyqr.dialogs.YesNoQuestionDialog;
import com.thegoodhealthnetwork.emergencyqr.global.Country;
import com.thegoodhealthnetwork.emergencyqr.intentservices.ConfirmIntentService;
import com.thegoodhealthnetwork.emergencyqr.intentservices.RegistrationIntentService;
import com.thegoodhealthnetwork.emergencyqr.model.SubscribeInformationModel;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.provider.ContactsContract;
import android.text.method.LinkMovementMethod;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

public class RegistrationActivity extends Activity {
	private EditText firstNameEdit;
	private EditText lastNameEdit;
	private EditText zipCodeEdit;
	private EditText countryEdit;
	private EditText emailEdit;
	private EditText phoneEdit;

	public static final int PICK_CONTACT = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_registration);

		String countryCode = getResources().getConfiguration().locale.getCountry();

		FrameLayout saveButton = (FrameLayout) findViewById(R.id.saveButton);
		saveButton.setOnClickListener(new SaveButtonCLickListener());

		firstNameEdit = (EditText) findViewById(R.id.firstNameEdit);
		lastNameEdit = (EditText) findViewById(R.id.lastNameEdit);
		zipCodeEdit = (EditText) findViewById(R.id.zipCodeEdit);
		zipCodeEdit
				.setOnEditorActionListener(new ZipCodeOnEditorActionListener(countryCode));

		countryEdit = (EditText) findViewById(R.id.countryEdit);
		countryEdit.setOnTouchListener(new CountryClickListener(countryCode));

		emailEdit = (EditText) findViewById(R.id.emailEdit);
		phoneEdit = (EditText) findViewById(R.id.phoneEdit);

		TextView privacyPolicyLink = (TextView)findViewById(R.id.privacyPolicyLink);
		privacyPolicyLink.setMovementMethod(LinkMovementMethod.getInstance());

		Button addressBookBtn = (Button) findViewById(R.id.addressBookBtn);
		addressBookBtn
				.setOnClickListener(new AddressBookButtonClickListener());

		setCountryNameFromCode(countryCode);

		InfoDialog infoDialog = new InfoDialog(this);
		infoDialog.setInfoText((String) getApplicationContext().getText(
				R.string.registrationInfoDialogText));
		infoDialog.show();
	}

	private void setCountryNameFromCode(String code) {
		countryEdit.setText(Country.getCountryName(this, code));
	}

	private void saveSubscribeInformation() {
		SubscribeInformationModel subscribeInformationModel = new SubscribeInformationModel();

		subscribeInformationModel.setFirstName(firstNameEdit.getText()
				.toString());
		subscribeInformationModel
				.setLastName(lastNameEdit.getText().toString());
		subscribeInformationModel.setZipCode(zipCodeEdit.getText().toString());
		subscribeInformationModel.setCountry(countryEdit.getText().toString());
		subscribeInformationModel.setEmail(emailEdit.getText().toString());
		subscribeInformationModel.setPhone(phoneEdit.getText().toString());

		SubscribeInformationDao subscribeInformationDao = new SubscribeInformationDao();
		try {
			subscribeInformationDao.save(subscribeInformationModel);

			sendSubscriptionInformation(subscribeInformationModel);

			/*Toast.makeText(this, "Subscribe information saved",
					Toast.LENGTH_LONG).show();*/

		} catch (Exception e) {
			Toast.makeText(
					this,
					"Error. Can't save subscribe information. "
							+ e.getMessage(), Toast.LENGTH_LONG).show();
		}
	}

	private void sendSubscriptionInformation(
			SubscribeInformationModel subscribeInformationModel) {

		if(isConnectingToInternet()) {
			Intent intent = new Intent(getApplicationContext(), RegistrationIntentService.class);

			Messenger messenger = new Messenger(
					new SendRegistrationResponseHandler());

			intent.putExtra("MESSENGER", messenger);
			intent.putExtra("subscribeInformationModel", subscribeInformationModel);

			startService(intent);
		}
		else {
			ErrorDialog errorDialog = new ErrorDialog(
					RegistrationActivity.this, "No connection to the Internet!");
			errorDialog.show();
		}

	}

	private void updateAppToken(String appToken) {
		SubscribeInformationDao subscribeInformationDao = new SubscribeInformationDao();

		try {
			SubscribeInformationModel model = subscribeInformationDao.load();
			model.setAppToken(appToken);

			subscribeInformationDao.save(model);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean isConnectingToInternet(){
		ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null)
		{
			NetworkInfo[] infos = connectivity.getAllNetworkInfo();
			if (infos != null)
				for (NetworkInfo info : infos)
					if (info.getState() == NetworkInfo.State.CONNECTED)
					{
						return true;
					}
		}
		return false;
	}


	private class SaveButtonCLickListener implements
			android.view.View.OnClickListener {

		@Override
		public void onClick(View v) {
			if (validate()) {
				saveSubscribeInformation();
			} else {
				ErrorDialog errorDialog = new ErrorDialog(
						RegistrationActivity.this, "All fields must be filled");
				errorDialog.show();
			}
		}
	}

	private boolean validate() {
		boolean valid = true;

		if (firstNameEdit.getText().toString().isEmpty()) {
			valid = false;
		} else if (lastNameEdit.getText().toString().isEmpty()) {
			valid = false;
		} else if (zipCodeEdit.getText().toString().isEmpty()) {
			valid = false;
		} else if (countryEdit.getText().toString().isEmpty()) {
			valid = false;
		} else if (emailEdit.getText().toString().isEmpty()) {
			valid = false;
		} else if (phoneEdit.getText().toString().isEmpty()) {
			valid = false;
		}

		return valid;
	}

	private class SendRegistrationResponseHandler extends Handler {

		public SendRegistrationResponseHandler() {
			super();

		}

		public void handleMessage(Message message) {
			if (message.arg1 == RegistrationIntentService.SUCCESS) {
				String appToken = (String) message.obj;
				if (appToken != null && !appToken.isEmpty()) {
					updateAppToken(appToken);
				}


				InfoDialog1 infoDialog1 = new InfoDialog1(RegistrationActivity.this);
				infoDialog1.setInfoText(getResources().getString(R.string.registrationNotification));
				infoDialog1.setInfoDialog1Listener(new InfoDialog1.InfoDialog1Listener() {
					@Override
					public void nextClick() {
						Intent i = new Intent(
								"com.thegoodhealthnetwork.emergencyqr.activity.MainActivity")
								.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
										| Intent.FLAG_ACTIVITY_CLEAR_TASK);

						startActivity(i);
					}
				});

				infoDialog1.setButtonText("Ok");
				infoDialog1.show();
			}
			else {
				int errorCode = message.arg2;

				if(errorCode!=4) {
					InfoDialog1 infoDialog1 = new InfoDialog1(RegistrationActivity.this);
					infoDialog1.setInfoText(getResources().getString(R.string.registrationError1));
					infoDialog1.setButtonText("Ok");
					infoDialog1.setInfoDialog1Listener(new InfoDialog1.InfoDialog1Listener() {
						@Override
						public void nextClick() {
							Intent i = new Intent(
									"com.thegoodhealthnetwork.emergencyqr.activity.MainActivity")
									.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
											| Intent.FLAG_ACTIVITY_CLEAR_TASK);

							startActivity(i);
						}
					});
					infoDialog1.show();
				}
				else if(errorCode==4) {
					YesNoQuestionDialog yesNoQuestionDialog = new YesNoQuestionDialog(RegistrationActivity.this,"Notification",getResources().getString(R.string.registrationError2));
					yesNoQuestionDialog.setYesNoQuestionDialogListener(new YesNoQuestionDialog.YesNoQuestionDialogListener() {
						@Override
						public void onYesClick() {
							startConfirmService();
						}

						@Override
						public void onNoClick() {
							RegistrationActivity.this.finish();
						}
					});
					yesNoQuestionDialog.setYesButtonText("Yes");
					yesNoQuestionDialog.setNoButtonText("No");

					yesNoQuestionDialog.show();
				}
			}


		}
	}

	private void startConfirmService() {
		Intent intent = new Intent(RegistrationActivity.this,ConfirmIntentService.class);

		Messenger messenger = new Messenger(
				new SendConfirmResponseHandler());

		intent.putExtra("MESSENGER", messenger);
		intent.putExtra("email", emailEdit.getText().toString());

		startService(intent);
	}


	private class SendConfirmResponseHandler extends Handler {

		public SendConfirmResponseHandler() {
			super();


		}

		public void handleMessage(Message message) {
			if (message.arg1 == ConfirmIntentService.SUCCESS) {
				String appToken = (String) message.obj;
				if (appToken != null && !appToken.isEmpty()) {
					updateAppToken(appToken);
				}

				Intent i = new Intent(
						"com.thegoodhealthnetwork.emergencyqr.activity.MainActivity")
						.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
								| Intent.FLAG_ACTIVITY_CLEAR_TASK);

				startActivity(i);
			} else {

				ErrorDialog errorDialog = new ErrorDialog(RegistrationActivity.this,(String)message.obj);
				errorDialog.setErrorDialogOkListener(new ErrorDialog.ErrorDialogOkListener() {
					@Override
					public void onOkClick() {

					}
				});
				errorDialog.show();

			}
		}
	}

	private void showCountriesDialog(String countryCode) {
		CountriesDialog countriesDialog = new CountriesDialog(
				RegistrationActivity.this,countryCode);

		countriesDialog
				.setCountriesDialogListener(new CountriesDialogListener() {

					@Override
					public void onOkClick(String selectedCountry) {
						countryEdit.setText(selectedCountry);
					}
				});

		countriesDialog.show();
	}

	private class CountryClickListener implements OnTouchListener {
		private String countryCode;

		public CountryClickListener(String countryCode) {
			this.countryCode = countryCode;
		}

		@Override
		public boolean onTouch(View v, MotionEvent event) {

			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				showCountriesDialog(this.countryCode);
			}
			return true;

		}
	}

	private class ZipCodeOnEditorActionListener implements
			OnEditorActionListener {
		private String countryCode;

		public ZipCodeOnEditorActionListener(String countryCode) {
			this.countryCode = countryCode;
		}

		@Override
		public boolean onEditorAction(TextView arg0, int action, KeyEvent arg2) {

			if (EditorInfo.IME_ACTION_NEXT == action) {
				showCountriesDialog(this.countryCode);
			}

			return false;
		}

	}

	private class AddressBookButtonClickListener implements
			OnClickListener {

		@Override
		public void onClick(View v) {
			Intent intent = new Intent(Intent.ACTION_PICK);
			intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
			startActivityForResult(intent, PICK_CONTACT);

		}

	}

	@Override
	public void onActivityResult(int reqCode, int resultCode, Intent data) {
		super.onActivityResult(reqCode, resultCode, data);

		switch (reqCode) {
			case (PICK_CONTACT):
				if (resultCode == Activity.RESULT_OK) {
					getContactDataFromAddressBook(data);
				}
				break;
		}
	}


	private void getContactDataFromAddressBook(Intent data) {

		Uri contactData = data.getData();
		Cursor cursor = getContentResolver().query(contactData, null, null,
				null, null);
		if (cursor.moveToFirst()) {
			String contactId = cursor.getString(cursor
					.getColumnIndex(ContactsContract.Contacts._ID));

			// String dsiplayName =
			// cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME));
			String dsiplayName = cursor.getString(cursor
					.getColumnIndex("display_name"));

			if(dsiplayName!=null) {
				String[] nameParts = dsiplayName.trim().split(" ");

				if(nameParts.length==2) {
					firstNameEdit.setText(nameParts[0]);
					lastNameEdit.setText(nameParts[1]);
				}
			}
			else {
				firstNameEdit.setText(dsiplayName);
			}



			String hasPhone = cursor
					.getString(cursor
							.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

			if (hasPhone.equalsIgnoreCase("1"))
				hasPhone = "true";
			else
				hasPhone = "false";

			if (Boolean.parseBoolean(hasPhone)) {
				Cursor phones = getContentResolver().query(
						ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
						null,
						ContactsContract.CommonDataKinds.Phone.CONTACT_ID
								+ " = " + contactId, null, null);
				while (phones.moveToNext()) {
					String phoneNumber = phones
							.getString(phones
									.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

					phoneEdit.setText(phoneNumber);

				}
				phones.close();
			}

			Cursor emails = getContentResolver().query(
					ContactsContract.CommonDataKinds.Email.CONTENT_URI,
					null,
					ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = "
							+ contactId, null, null);
			while (emails.moveToNext()) {
				String emailAddress = emails
						.getString(emails
								.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
				emailEdit.setText(emailAddress);

			}
			emails.close();
		}
		cursor.close();
	}
}
