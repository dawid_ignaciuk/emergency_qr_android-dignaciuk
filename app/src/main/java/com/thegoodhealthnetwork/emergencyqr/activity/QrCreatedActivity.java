package com.thegoodhealthnetwork.emergencyqr.activity;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import com.thegoodhealthnetwork.emergencyqr.R;
import com.thegoodhealthnetwork.emergencyqr.dao.AppDataDao;
import com.thegoodhealthnetwork.emergencyqr.dialogs.ActionsDialog;
import com.thegoodhealthnetwork.emergencyqr.dialogs.ActionsDialog.ActionsDialogListener;
import com.thegoodhealthnetwork.emergencyqr.dialogs.InfoDontShowDialog;
import com.thegoodhealthnetwork.emergencyqr.dialogs.InfoDontShowDialog.InfoDontShowDialogListener;
import com.thegoodhealthnetwork.emergencyqr.dialogs.SetAsLockScreenDialog;
import com.thegoodhealthnetwork.emergencyqr.dialogs.ShareQuestionDialog;
import com.thegoodhealthnetwork.emergencyqr.dialogs.ShareQuestionDialog.ShareQuestionListener;
import com.thegoodhealthnetwork.emergencyqr.generator.QrCodeImageGenerator;
import com.thegoodhealthnetwork.emergencyqr.model.AppDataModel;
import com.thegoodhealthnetwork.emergencyqr.model.QRCodeDataModel;
import com.thegoodhealthnetwork.emergencyqr.satatistic.StatisticManager;

public class QrCreatedActivity extends Activity {
	private ImageView qrCodeImage;
	private ActionsDialog actionsDialog;
	private Bitmap qrCodeImageBitmap;
	private TextView phoneOwnerNameValueTextView;
	private TextView phoneOwnerEmailValueTextView;
	private TextView emergencyContactNameValueTextView;
	private TextView emergencyContactPhoneValueTextView;
	private TextView emergencyContactEmailValueTextView;
	private TextView emergencyContactRelationShipValueTextView;
	private TextView isHealthProxyValueTextView;
	private TextView medicationsValueTextView;
	private TextView conditionValueTextView;
	private TextView allergiesValueTextView;
	private TextView otherValueTextView;
	
	private QRCodeDataModel qrcodedata;

	private static final int START_COMPOSER_ACTIVITY = 100;
	private static final int START_PREVIEW_ACTIVITY = 200;
	private static final int START_SHARE_ACTIVITY = 400;
	private static final int START_SETTINGS_ACTIVITY = 800;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		qrcodedata = (QRCodeDataModel)getIntent().getSerializableExtra("qrcodedata");
		
		setContentView(R.layout.activity_qr_created);

		Button actionsButton = (Button) findViewById(R.id.actionsBtn);
		actionsButton.setOnClickListener(new ActionsButtonCLickListener());
		
		qrCodeImage = (ImageView)findViewById(R.id.qrCodeImage);
		
		phoneOwnerNameValueTextView = (TextView)findViewById(R.id.phoneOwnerNameValueTextView);
		phoneOwnerEmailValueTextView = (TextView)findViewById(R.id.phoneOwnerEmailValueTextView);
		
		emergencyContactNameValueTextView = (TextView)findViewById(R.id.emergencyContactNameValueTextView);
		emergencyContactPhoneValueTextView = (TextView)findViewById(R.id.emergencyContactPhoneValueTextView);
		emergencyContactEmailValueTextView = (TextView)findViewById(R.id.emergencyContactEmailValueTextView);

		emergencyContactRelationShipValueTextView = (TextView)findViewById(R.id.emergencyContactRelationShipValueTextView);
		
		isHealthProxyValueTextView = (TextView)findViewById(R.id.isHealthProxyValueTextView);
		
		medicationsValueTextView = (TextView)findViewById(R.id.medicationsValueTextView);
		
		conditionValueTextView = (TextView)findViewById(R.id.conditionValueTextView);
		
		allergiesValueTextView = (TextView)findViewById(R.id.allergiesValueTextView);
		
		otherValueTextView = (TextView)findViewById(R.id.otherValueTextView);
		
		generateQrCode(qrcodedata);
		
		loadQrCodeData(qrcodedata);
		
		try {
			showInfoDialog();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void loadQrCodeData(QRCodeDataModel qrCodeDataModel) {
		phoneOwnerNameValueTextView.setText(qrCodeDataModel.getPhonneOwnerName());
		phoneOwnerEmailValueTextView.setText(qrCodeDataModel.getPhoneOwnerEmail());
		
		emergencyContactNameValueTextView.setText(qrCodeDataModel.getEmergencyPersonName());
		emergencyContactPhoneValueTextView.setText(qrCodeDataModel.getEmergencyPersonPhone());
		emergencyContactEmailValueTextView.setText(qrCodeDataModel.getEmergencyPersonEmail());
		emergencyContactRelationShipValueTextView.setText(qrCodeDataModel.getEmergencyPersonRelationShip());
		
		if(qrCodeDataModel.isHealthProxy()) {
			isHealthProxyValueTextView.setText(getResources().getString(R.string.yes));
		}
		else {
			isHealthProxyValueTextView.setText(getResources().getString(R.string.no));
		}
		
		if(qrCodeDataModel.getMedications()!=null && !qrCodeDataModel.getMedications().trim().equals(getString(R.string.emptyValue))) {
			medicationsValueTextView.setText(qrCodeDataModel.getMedications());
		}
		else {
			medicationsValueTextView.setText(getApplicationContext().getText(R.string.noneIndicated));
		}
		
		if(qrCodeDataModel.getCondition()!=null && !qrCodeDataModel.getCondition().trim().equals(getString(R.string.emptyValue))) {
			conditionValueTextView.setText(qrCodeDataModel.getCondition());
		}
		else {
			conditionValueTextView.setText(getApplicationContext().getText(R.string.noneIndicated));
		}
		
		if(qrCodeDataModel.getAllergies()!=null && !qrCodeDataModel.getAllergies().trim().equals(getString(R.string.emptyValue))) {
			allergiesValueTextView.setText(qrCodeDataModel.getAllergies());
		}
		else {
			allergiesValueTextView.setText(getApplicationContext().getText(R.string.noneIndicated));
		}
		
		if(qrCodeDataModel.getOther()!=null && !qrCodeDataModel.getOther().trim().equals(getString(R.string.emptyValue))) {
			otherValueTextView.setText(qrCodeDataModel.getOther());
		}
		else {
			otherValueTextView.setText(getApplicationContext().getText(R.string.noneIndicated));
		}
	}
	
	private void generateQrCode(QRCodeDataModel qRCodeDataModel) {
		QrCodeImageGenerator qrCodeImageGenerator = new QrCodeImageGenerator(this);
		qrCodeImageBitmap = qrCodeImageGenerator.generateCode(qRCodeDataModel.toString(),false);
		
		qrCodeImage.setImageBitmap(qrCodeImageBitmap);
	}
	
	
	private class ActionsButtonCLickListener implements android.view.View.OnClickListener {

		@Override
		public void onClick(View arg0) {
			showActionDialog();
		}
		
	}
	
	private class ActionsDialogListenerImpl implements ActionsDialogListener {

		@Override
		public void onSendClick() throws Exception {

			StatisticManager.increaseOurCodeSend(new Date(System.currentTimeMillis()));

			ArrayList<Uri> imagesUris = new ArrayList<>();
			String pathToImage = saveQrCodeImageToFile();
			File imageFile = new File(pathToImage);
			Uri uri = Uri.parse("file://" + imageFile.getAbsolutePath());
			imagesUris.add(uri);

			final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND_MULTIPLE);
			emailIntent.putExtra(Intent.EXTRA_STREAM,imagesUris);
			emailIntent.setType("image/*");
			emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, QrCreatedActivity.this.getString(R.string.qrCodeEmailSubject));
			emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, createEmailContent(QrCreatedActivity.this.getString(R.string.qrCodeEmailBody)));
			startActivityForResult(Intent.createChooser(emailIntent,"Send email by ..."), 1);
		}

		@Override 
		public void onSaveClick() throws Exception {

			/*AppDataDao appDataDao = new AppDataDao();

			AppDataModel appDataModel = appDataDao.load();

			if(!appDataModel.isDontShowCreateMessage3() && appDataModel.getCreateMessage3NuberOfShow()<5) {
				InfoDontShowDialog infoDontShowDialog = new InfoDontShowDialog(QrCreatedActivity.this);
				infoDontShowDialog.setInfoTitle((String) QrCreatedActivity.this.getText(R.string.createQrCodeSaveTitle));
				infoDontShowDialog.setInfoText((String) QrCreatedActivity.this.getText(R.string.createQrCodeSaveMessage));
				infoDontShowDialog.setInfoDontShowDialogListener(new InfoDontShowDialogListener() {
					@Override
					public void onDontShowClick() {
						saveMessageCounter3();
						saveQrCodeImage();
					}

					@Override
					public void onOkClick() {
						increadeMessageCounter3();
						saveQrCodeImage();
					}
				});
				infoDontShowDialog.show();
			}
			else {
				saveQrCodeImage();
			}*/

			saveQrCodeImage();

			SetAsLockScreenDialog setAsLockScreenDialog = new SetAsLockScreenDialog(QrCreatedActivity.this);

			setAsLockScreenDialog.setSetAsLockScreenDialogListener(new SetAsLockScreenDialog.SetAsLockScreenDialogListener() {
				@Override
				public void onSetLoockScreenClick() {
					Intent intent = new Intent(Settings.ACTION_SETTINGS);
					startActivityForResult(intent,START_SETTINGS_ACTIVITY);
				}

				@Override
				public void onShareAppClick() {
					Intent intent = new Intent("com.thegoodhealthnetwork.emergencyqr.activity.QrShareActivity");
					startActivityForResult(intent,START_SHARE_ACTIVITY);
				}
			});

			setAsLockScreenDialog.show();

		}

		@Override
		public void onPrintClick() {
			
		}

		@Override
		public void onShareClick() {
			actionsDialog.cancel();
			startShareActivity();
		}

		@Override
		public void onPreviewClick() {
			Intent intent = new Intent(
					"com.thegoodhealthnetwork.emergencyqr.activity.QrPreviewActivity");
			intent.putExtra("qrcodedata", qrcodedata);
			startActivityForResult(intent,START_PREVIEW_ACTIVITY);
		}

		@Override
		public void onComposeClick() {
			Intent intent = new Intent(
					"com.thegoodhealthnetwork.emergencyqr.activity.ComposerActivity");
			intent.putExtra("qrcodedata", qrcodedata);
			startActivityForResult(intent,START_COMPOSER_ACTIVITY);
		}
	}



	
	private String saveQrCodeImageToFile() {

		SimpleDateFormat sdf = new SimpleDateFormat("d-M-y h:m:s");

		QrCodeImageGenerator qrCodeImageGenerator = new QrCodeImageGenerator(this);
		Bitmap qrCodeWithBackground = qrCodeImageGenerator.generateCodeForEmail(qrcodedata.toString());
		
		qrCodeImage.setImageBitmap(qrCodeImageBitmap);
		
		String pathToFolder = Environment.getExternalStoragePublicDirectory(getResources().getString(R.string.app_folder_name)).getAbsolutePath();
		
		File folderFile = new File(pathToFolder);

		if(!folderFile.exists()) {
			folderFile.mkdir();
		}
		
		File qrcodepromotion = new File(folderFile, "emergency-qr-"+sdf.format(new Date(System.currentTimeMillis()))+".png");

		try {
			FileOutputStream fos = new FileOutputStream(qrcodepromotion);
			qrCodeWithBackground.compress(Bitmap.CompressFormat.PNG, 100, fos);
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return qrcodepromotion.getAbsolutePath();
	}
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if(requestCode == START_PREVIEW_ACTIVITY) {
			if(resultCode == ComposerActivity.COMPOSER_ACTIVITY_CANCELED || resultCode == QrShareActivity.SHARE_ACTIVITY_CANCELED) {
				showActionDialog();
			}
		}
		else if(requestCode !=START_SETTINGS_ACTIVITY && requestCode!=START_COMPOSER_ACTIVITY && requestCode!=START_SHARE_ACTIVITY) {
			showShareQuestion();
		}
	}
	
	private void showShareQuestion() {
		ShareQuestionDialog shareQuestionDialog = new ShareQuestionDialog(QrCreatedActivity.this);
		shareQuestionDialog.setShareQuestionListener(new ShareQuestionListenerImpl());
		shareQuestionDialog.show();
	}

	private class ShareQuestionListenerImpl implements ShareQuestionListener {

		@Override
		public void onYesClick() {
			startShareActivity();
		}
	}
	
	private void startShareActivity() {
		Intent intent = new Intent("com.thegoodhealthnetwork.emergencyqr.activity.QrShareActivity");
		startActivityForResult(intent,START_SHARE_ACTIVITY);
	}
	
	
	private void showInfoDialog() throws Exception {
		AppDataDao appDataDao = new AppDataDao();
		
		AppDataModel appDataModel = appDataDao.load();
		
		if(appDataModel!= null && !appDataModel.isDontShowCreateMessage2()) {
			
			InfoDontShowDialog infoDontShowDialog = new InfoDontShowDialog(QrCreatedActivity.this);
			infoDontShowDialog.setInfoTitle((String)QrCreatedActivity.this.getText(R.string.createQrCodeTitle));
			infoDontShowDialog.setInfoText((String)QrCreatedActivity.this.getText(R.string.createQrCodeMessage));
			infoDontShowDialog.setInfoDontShowDialogListener(new InfoDontShowDialogListenerImpl());
			infoDontShowDialog.show();
		}
	}
	
	
	private class InfoDontShowDialogListenerImpl implements InfoDontShowDialogListener {

		@Override
		public void onDontShowClick() {
			saveMessageCounter(true);
		}

		@Override
		public void onOkClick() {
			saveMessageCounter(false);
		}
	}

	
	private void saveMessageCounter(boolean disableMessage) {
		AppDataDao appDataDao = new AppDataDao();
		
		AppDataModel appDataModel;
		try {
			appDataModel = appDataDao.load();
			
			appDataModel.setDontShowCreateMessage2(disableMessage);
			appDataModel.setCreateMessage2NuberOfShow(appDataModel.getCreateMessage2NuberOfShow() + 1);
			
			appDataDao.save(appDataModel);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void saveMessageCounter3() {
		AppDataDao appDataDao = new AppDataDao();

		AppDataModel appDataModel;
		try {
			appDataModel = appDataDao.load();

			appDataModel.setDontShowCreateMessage3(true);
			appDataModel.increaseMessage3Counter();

			appDataDao.save(appDataModel);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void increadeMessageCounter3() {
		AppDataDao appDataDao = new AppDataDao();

		AppDataModel appDataModel;
		try {
			appDataModel = appDataDao.load();

			appDataModel.increaseMessage3Counter();

			appDataDao.save(appDataModel);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void saveQrCodeImage() {
		StatisticManager.increaseOurCodeSaved(new Date(System.currentTimeMillis()));

		String filePath = saveQrCodeImageToFile();
		ContentValues values = new ContentValues();

		values.put(Images.Media.DATE_TAKEN, System.currentTimeMillis());
		values.put(Images.Media.MIME_TYPE, "image/png");
		values.put(MediaStore.MediaColumns.DATA, filePath);

		getApplicationContext().getContentResolver().insert(Images.Media.EXTERNAL_CONTENT_URI, values);

		Toast.makeText(getApplicationContext(), "Image saved to gallery", Toast.LENGTH_LONG).show();

		//showShareQuestion();
	}


	private String createEmailContent(String emailText) {
		StringBuilder sb = new StringBuilder();

		sb.append(emailText);
		sb.append("\n\n");
		sb.append("Your QR code contains the following  information:\n");

		sb.append("Phone owner\n");
		sb.append("Name: ").append(qrcodedata.getPhonneOwnerName()).append("\n");
		sb.append("E-mail: ").append(qrcodedata.getPhoneOwnerEmail()).append("\n");
		sb.append("Emergency contact\n");
		sb.append("Name: ").append(qrcodedata.getEmergencyPersonName()).append("\n");
		sb.append("Phone: ").append(qrcodedata.getEmergencyPersonPhone()).append("\n");
		sb.append("E-mail: ").append(qrcodedata.getEmergencyPersonEmail()).append("\n");
		sb.append("Relationship: ").append(qrcodedata.getEmergencyPersonRelationShip()).append("\n");
		sb.append("Health care proxy: ");

		if(qrcodedata.isHealthProxy()) {
			sb.append("YES\n");
		}
		else {
			sb.append("NO\n");
		}

		sb.append("Health information\n");

		sb.append("Medications: ");
		if(qrcodedata.getMedications()!=null && !qrcodedata.getMedications().isEmpty()) {
			sb.append(qrcodedata.getMedications()).append("\n");
		}
		else {
			sb.append("none indicated\n");
		}

		sb.append("Conditions: ");
		if(qrcodedata.getCondition()!=null && !qrcodedata.getCondition().isEmpty()) {
			sb.append(qrcodedata.getCondition()).append("\n");
		}
		else {
			sb.append("none indicated\n");
		}

		sb.append("Allergies: ");
		if(qrcodedata.getAllergies()!=null && !qrcodedata.getAllergies().isEmpty()) {
			sb.append(qrcodedata.getAllergies()).append("\n");
		}
		else {
			sb.append("none indicated\n");
		}

		sb.append("Notes: ");
		if(qrcodedata.getOther()!=null && !qrcodedata.getOther().isEmpty()) {
			sb.append(qrcodedata.getOther()).append("\n");
		}
		else {
			sb.append("none indicated\n");
		}

		return sb.toString();
	}


	private void showActionDialog() {
		actionsDialog = new ActionsDialog(QrCreatedActivity.this);
		actionsDialog.setActionsDialogListener(new ActionsDialogListenerImpl());
		actionsDialog.show();
	}
		
}
