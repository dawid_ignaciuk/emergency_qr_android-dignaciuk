package com.thegoodhealthnetwork.emergencyqr.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.TextView;
import java.util.Date;
import com.thegoodhealthnetwork.emergencyqr.R;
import com.thegoodhealthnetwork.emergencyqr.dialogs.TextViewDialog;
import com.thegoodhealthnetwork.emergencyqr.model.QRCodeDataModel;
import com.thegoodhealthnetwork.emergencyqr.satatistic.StatisticManager;

public class QrDecodedActivity extends Activity {
	
	private static final String TAG = "QREncoderMessageActivity";
	
	private TextView phoneOwnerNameValueTextView;
	private TextView phoneOwnerEmailValueTextView;
	private TextView emergencyContactNameValueTextView;
	private TextView emergencyContactPhoneValueTextView;
	private TextView emergencyContactRelationShiplValueTextView;

	private TextView emergencyContactEmailValueTextView;
	private TextView isHealthProxyValueTextView;
	
	private TextView medicationsValueTextView;
	private TextView conditionValueTextView;
	private TextView allergiesValueTextView;
	private TextView otherValueTextView;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_qr_decoded);
		
		StatisticManager.increaseOurCodeRead(new Date(System.currentTimeMillis()));
		
		phoneOwnerNameValueTextView = (TextView)findViewById(R.id.phoneOwnerNameValueTextView);
		phoneOwnerEmailValueTextView = (TextView)findViewById(R.id.phoneOwnerEmailValueTextView);
		
		emergencyContactNameValueTextView = (TextView)findViewById(R.id.emergencyContactNameValueTextView);
		emergencyContactPhoneValueTextView = (TextView)findViewById(R.id.emergencyContactPhoneValueTextView);
		emergencyContactEmailValueTextView = (TextView)findViewById(R.id.emergencyContactEmailValueTextView);
		emergencyContactRelationShiplValueTextView = (TextView)findViewById(R.id.emergencyContactRelationShiplValueTextView);
		
		isHealthProxyValueTextView = (TextView)findViewById(R.id.isHealthProxyValueTextView);
		
		medicationsValueTextView = (TextView)findViewById(R.id.medicationsValueTextView);
		medicationsValueTextView.setOnClickListener(new MedicationClickListener());
		
		conditionValueTextView = (TextView)findViewById(R.id.conditionValueTextView);
		conditionValueTextView.setOnClickListener(new ConditionClickListener());
		
		allergiesValueTextView = (TextView)findViewById(R.id.allergiesValueTextView);
		allergiesValueTextView.setOnClickListener(new AllergiesClickListener());
		
		otherValueTextView = (TextView)findViewById(R.id.otherValueTextView);
		otherValueTextView.setOnClickListener(new OthersClickListener());
		
		FrameLayout phoneOwnerEmailValueFrame = (FrameLayout)findViewById(R.id.phoneOwnerEmailValueFrame);
		phoneOwnerEmailValueFrame.setOnClickListener(new OwnerEmailClickListener());
		
		
		FrameLayout emergencyContactEmailFrame = (FrameLayout)findViewById(R.id.emergencyContactEmailFrame);
		emergencyContactEmailFrame.setOnClickListener(new EmergencyContactEmailClickListener());
		
		QRCodeDataModel qrcodedata = (QRCodeDataModel)getIntent().getSerializableExtra("qrcodedata");
		
		loadQrCodeData(qrcodedata);
	}
	
	private void loadQrCodeData(QRCodeDataModel qrCodeDataModel) {
		phoneOwnerNameValueTextView.setText(qrCodeDataModel.getPhonneOwnerName());
		phoneOwnerEmailValueTextView.setText(qrCodeDataModel.getPhoneOwnerEmail());
		
		emergencyContactNameValueTextView.setText(qrCodeDataModel.getEmergencyPersonName());
		emergencyContactPhoneValueTextView.setText(qrCodeDataModel.getEmergencyPersonPhone());
		emergencyContactEmailValueTextView.setText(qrCodeDataModel.getEmergencyPersonEmail());
		emergencyContactRelationShiplValueTextView.setText(qrCodeDataModel.getEmergencyPersonRelationShip());
		
		if(qrCodeDataModel.isHealthProxy()) {
			isHealthProxyValueTextView.setText("Yes");
		}
		else {
			isHealthProxyValueTextView.setText("No");
		}
		
		if(qrCodeDataModel.getMedications()!=null && !qrCodeDataModel.getMedications().trim().equals(getString(R.string.emptyValue))) {
			medicationsValueTextView.setText(qrCodeDataModel.getMedications());
		}
		else {
			medicationsValueTextView.setText(getString(R.string.emptyValue));
		}
		
		if(qrCodeDataModel.getCondition()!=null && !qrCodeDataModel.getCondition().trim().equals(getString(R.string.emptyValue))) {
			conditionValueTextView.setText(qrCodeDataModel.getCondition());
		}
		else {
			conditionValueTextView.setText(getString(R.string.emptyValue));
		}
		
		if(qrCodeDataModel.getAllergies()!=null && !qrCodeDataModel.getAllergies().trim().equals(getString(R.string.emptyValue))) {
			allergiesValueTextView.setText(qrCodeDataModel.getAllergies());
		}
		else {
			allergiesValueTextView.setText(getString(R.string.emptyValue));
		}
		
		if(qrCodeDataModel.getOther()!=null && !qrCodeDataModel.getOther().trim().equals(getString(R.string.emptyValue))) {
			otherValueTextView.setText(qrCodeDataModel.getOther());
		}
		else {
			otherValueTextView.setText(getString(R.string.emptyValue));
		}
	}
	
	private class MedicationClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			TextViewDialog textViewDialog = new TextViewDialog(QrDecodedActivity.this, "Medications", medicationsValueTextView.getText().toString());
			textViewDialog.show();
			
		}
		
	}
	
	private class ConditionClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			TextViewDialog textViewDialog = new TextViewDialog(QrDecodedActivity.this, "Condition", conditionValueTextView.getText().toString());
			textViewDialog.show();
			
		}
		
	}
	
	private class AllergiesClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			TextViewDialog textViewDialog = new TextViewDialog(QrDecodedActivity.this, "Allergies", allergiesValueTextView.getText().toString());
			textViewDialog.show();
			
		}
	}
	
	private class OthersClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			TextViewDialog textViewDialog = new TextViewDialog(QrDecodedActivity.this, getString(R.string.other), otherValueTextView.getText().toString());
			textViewDialog.show();
			
		}
	}
	
	private class OwnerEmailClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			if(!phoneOwnerEmailValueTextView.getText().toString().isEmpty()) {
			 final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
			 
			 	String[] address = {phoneOwnerEmailValueTextView.getText().toString()};
				
		      
		        emailIntent.putExtra(Intent.EXTRA_TEXT, QrDecodedActivity.this.getString(R.string.emailToOwnerBody));
		        emailIntent.putExtra(Intent.EXTRA_SUBJECT, QrDecodedActivity.this.getString(R.string.emailToOwnerSubject));
		        emailIntent.putExtra(Intent.EXTRA_EMAIL,address);
		        emailIntent.setType("text/plain");
		        startActivity(Intent.createChooser(emailIntent,"Send email by ..."));
			}
		}
	}
	
	private class EmergencyContactEmailClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			if(!emergencyContactEmailValueTextView.getText().toString().isEmpty()) {
				final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
		      
				String[] address = {emergencyContactEmailValueTextView.getText().toString()};
				
		        emailIntent.putExtra(Intent.EXTRA_TEXT, QrDecodedActivity.this.getString(R.string.emailToEmergencyContactBody));
		        emailIntent.putExtra(Intent.EXTRA_SUBJECT, QrDecodedActivity.this.getString(R.string.emailToEmergencyContactSubject));
		        emailIntent.putExtra(Intent.EXTRA_EMAIL,address);
		        emailIntent.setType("text/plain");
		        startActivity(Intent.createChooser(emailIntent,"Send email by ..."));
			}
		}
	}
}
