package com.thegoodhealthnetwork.emergencyqr.global;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import android.content.Context;

public class Country {

	public static List<String> getCountries(Context context) {

		List<String> countries = new ArrayList<>();

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					context.getAssets().open("countries.csv")));

			String mLine = reader.readLine();
			while (mLine != null) {
				
				String[] splits = mLine.split(";");

				if(splits.length==2) {
					countries.add(splits[1].trim());
				}

				mLine = reader.readLine();
			}
			reader.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		return countries;
	}
	
	public static String getCountryName(Context context,String countryCode) {
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					context.getAssets().open("countries.csv")));

			String mLine = reader.readLine();
			while (mLine != null) {
				
				String[] splits = mLine.split(";");

				if(splits.length==2) {
					if(countryCode.toUpperCase().equals(splits[0].toUpperCase())) {
						return splits[1];
					}
				}

				mLine = reader.readLine();
			}
			reader.close();
		
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}
}
