package com.thegoodhealthnetwork.emergencyqr.global;

import android.os.Parcelable;

/**
 * Created by kamil on 21.07.15.
 */
public class ComposerDataGLobal {

    private static float[] m;

    private static Parcelable image1;
    private static Parcelable image2;
    private static Parcelable image3;
    private static Parcelable image5;
    private static Parcelable image6;


    public static float[] getM() {
        return m;
    }

    public static void setM(float[] m) {
        ComposerDataGLobal.m = m;
    }

    public static Parcelable getImage1() {
        return image1;
    }

    public static void setImage1(Parcelable image1) {
        ComposerDataGLobal.image1 = image1;
    }

    public static Parcelable getImage2() {
        return image2;
    }

    public static void setImage2(Parcelable image2) {
        ComposerDataGLobal.image2 = image2;
    }

    public static Parcelable getImage3() {
        return image3;
    }

    public static void setImage3(Parcelable image3) {
        ComposerDataGLobal.image3 = image3;
    }

    public static Parcelable getImage5() {
        return image5;
    }

    public static void setImage5(Parcelable image5) {
        ComposerDataGLobal.image5 = image5;
    }

    public static Parcelable getImage6() {
        return image6;
    }

    public static void setImage6(Parcelable image6) {
        ComposerDataGLobal.image6 = image6;
    }
}
