package com.thegoodhealthnetwork.emergencyqr.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import com.thegoodhealthnetwork.emergencyqr.global.Global;
import com.thegoodhealthnetwork.emergencyqr.model.SubscribeInformationModel;
import android.util.Log;

public class SubscribeInformationDao {
	private File subscribeInformationDataFile;
	
	private static final String TAG = "SubscribeInformationDao";
	
	public SubscribeInformationDao() {

		String qrCodeDataFolder = "/qrcodedata";

		File qrCodeDataFolderFile = new File(
				Global.getIntegrnalApplicationFilePaht() + qrCodeDataFolder);

		if (!qrCodeDataFolderFile.exists()) {
			qrCodeDataFolderFile.mkdir();
		}

		subscribeInformationDataFile = new File(qrCodeDataFolderFile, "subscribe.dat");
	}

	public void save(SubscribeInformationModel subscribeInformationModel) throws Exception {

		try {
			FileOutputStream fos = new FileOutputStream(subscribeInformationDataFile);

			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(subscribeInformationModel);
			oos.close();
		} catch (IOException e) {
			Log.e(TAG,e.getMessage());
			Log.e(TAG,
					"Can't save qrcode data to file "
							+ subscribeInformationDataFile.getAbsolutePath());
			throw new Exception("Can't save data");
		}
	}
	
	public SubscribeInformationModel load() throws Exception {
		SubscribeInformationModel subscribeInformationModel = null;

		if (!subscribeInformationDataFile.exists()) {
			return subscribeInformationModel;
		}

		try {
			FileInputStream fis = new FileInputStream(subscribeInformationDataFile);

			ObjectInputStream ois = new ObjectInputStream(fis);
			subscribeInformationModel = (SubscribeInformationModel) ois.readObject();
			ois.close();

			return subscribeInformationModel;
		} catch (IOException e) {
			Log.e(TAG,e.getMessage());
			Log.e(TAG,
					"Can't read qrcode data from file "
							+ subscribeInformationDataFile.getAbsolutePath());
			throw new Exception("Can't read settings data");
		}
	}
}
