package com.thegoodhealthnetwork.emergencyqr.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import com.thegoodhealthnetwork.emergencyqr.global.Global;
import com.thegoodhealthnetwork.emergencyqr.model.AppDataModel;
import android.util.Log;

public class AppDataDao {
	private File appDataFile;
	
	private static final String TAG = "AppDataDao";

	public AppDataDao() {

		String qrCodeDataFolder = "/appdata";

		File qrCodeDataFolderFile = new File(
				Global.getIntegrnalApplicationFilePaht() + qrCodeDataFolder);

		if (!qrCodeDataFolderFile.exists()) {
			qrCodeDataFolderFile.mkdir();
		}

		appDataFile = new File(qrCodeDataFolderFile, "appdata.dat");
	}

	public void save(AppDataModel appDataModel) throws Exception {
	
		try {
			FileOutputStream fos = new FileOutputStream(appDataFile);

			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(appDataModel);
			oos.close();
		} catch (IOException e) {
			Log.e(TAG,e.getMessage());
			Log.e(TAG,
					"Can't save qrcode data to file "
							+ appDataFile.getAbsolutePath());
			throw new Exception("Can't save data");
		}
	}

	public AppDataModel load() throws Exception {
		AppDataModel appDataModel=null;

		if (!appDataFile.exists()) {
			return null;
		}

		try {
			FileInputStream fis = new FileInputStream(appDataFile);

			ObjectInputStream ois = new ObjectInputStream(fis);
			appDataModel = (AppDataModel) ois.readObject();
			ois.close();

			return appDataModel;
		} catch (IOException e) {
			Log.e(TAG,e.getMessage());
			Log.e(TAG,
					"Can't read app from file "
							+ appDataFile.getAbsolutePath());
			throw new Exception("Can't read app data");
		}
	}
}