package com.thegoodhealthnetwork.emergencyqr.intentservices;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;
import com.thegoodhealthnetwork.emergencyqr.R;
import com.thegoodhealthnetwork.emergencyqr.services.JsonPostService;

public class ConfirmIntentService extends IntentService {
	public final static int SUCCESS = 1;
	public final static int ERROR = 2;
	private String email;
	private Messenger messenger;

	public ConfirmIntentService() {
		super("ConfirmIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {

		Bundle extras = intent.getExtras();
		if (extras != null) {
			messenger = (Messenger) extras.get("MESSENGER");
			email = (String) extras
					.get("email");
		}

		Message msg = Message.obtain();

		try {

			String response = JsonPostService.makeRequest(getApplicationContext().getResources().getString(R.string.pathToConfirmService),
					getMapFromModel(email));
			JSONObject jsonObject = new JSONObject(response);


			int errorExist= 0;

			if(jsonObject.has("error")) {
				errorExist = jsonObject.getInt("error");
			}

			if(errorExist==0) {
				msg.arg1 = SUCCESS;
				msg.obj = jsonObject.get("app_token");
			}
			else {

				msg.arg1 = ERROR;
				msg.obj="Unknow error";
				JSONObject error = jsonObject.getJSONObject("error_list");

				if(error!=null) {
					msg.arg2 = error.getInt("code");
					msg.obj = error.getString("message");
				}
			}
		} catch (Exception e) {
			msg.arg1 = 2;
			e.printStackTrace();
		}

		try {
			messenger.send(msg);
		} catch (android.os.RemoteException e1) {
			Log.w(getClass().getName(), "Exception sending message", e1);
		}

	}

	private JSONObject getMapFromModel(String email) throws JSONException {
		JSONObject obj = new JSONObject();

		obj.put("email", email);
		obj.put("phoneType", "android");

		return obj;

	}

}
