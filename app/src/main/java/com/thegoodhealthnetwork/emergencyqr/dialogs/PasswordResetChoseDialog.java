package com.thegoodhealthnetwork.emergencyqr.dialogs;

import com.thegoodhealthnetwork.emergencyqr.R;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.FrameLayout;

public class PasswordResetChoseDialog extends Dialog {

	public PasswordResetChoseDialog(Context context) {
		super(context);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.password_reset_chose_view);

		this.setCancelable(false);

		getWindow().setLayout(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT);

		FrameLayout securityQestionButton = (FrameLayout) findViewById(R.id.securityQestionButton);
		securityQestionButton
				.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View arg0) {
						if (passwordResetChose != null) {
							passwordResetChose.onSecurityQuestion();

						}
						PasswordResetChoseDialog.this.cancel();
					}
				});

		FrameLayout emailResetButton = (FrameLayout) findViewById(R.id.emailResetButton);
		emailResetButton
				.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View arg0) {
						if (passwordResetChose != null) {
							passwordResetChose.onResetByEmail();

						}
						PasswordResetChoseDialog.this.cancel();
					}
				});

		FrameLayout cancelButton = (FrameLayout)findViewById(R.id.cancelButton);
		cancelButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				PasswordResetChoseDialog.this.cancel();
			}
		});
	}

	public interface PasswordResetChoseListener {
		public void onSecurityQuestion();

		public void onResetByEmail();
	}

	private PasswordResetChoseListener passwordResetChose;

	public void setPasswordResetChose(PasswordResetChoseListener passwordResetChose) {
		this.passwordResetChose = passwordResetChose;
	}
}
