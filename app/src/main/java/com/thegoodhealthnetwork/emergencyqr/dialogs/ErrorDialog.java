package com.thegoodhealthnetwork.emergencyqr.dialogs;

import com.thegoodhealthnetwork.emergencyqr.R;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.TextView;

public class ErrorDialog extends Dialog {

	public interface  ErrorDialogOkListener {
		public void onOkClick();
	}

	private ErrorDialogOkListener errorDialogOkListener;
		
	public ErrorDialog(Context context,String message) {
		super(context);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.error_view);

		setCanceledOnTouchOutside(false);
	
		getWindow().setLayout(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
		
		TextView errorMessageLabel = (TextView)findViewById(R.id.errorMessageLabel);
		errorMessageLabel.setText(message);
		
		FrameLayout okButton = (FrameLayout)findViewById(R.id.okButton);
		okButton.setOnClickListener(new OKButtonCLickListener());
	}
	
	
	private class OKButtonCLickListener implements android.view.View.OnClickListener {

		@Override
		public void onClick(View v) {

			if(errorDialogOkListener!=null) {
				errorDialogOkListener.onOkClick();
			}

			ErrorDialog.this.cancel();
		}
	}


	public ErrorDialogOkListener getErrorDialogOkListener() {
		return errorDialogOkListener;
	}

	public void setErrorDialogOkListener(ErrorDialogOkListener errorDialogOkListener) {
		this.errorDialogOkListener = errorDialogOkListener;
	}
}
