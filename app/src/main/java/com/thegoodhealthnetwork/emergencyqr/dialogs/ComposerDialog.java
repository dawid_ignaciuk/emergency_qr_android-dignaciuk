package com.thegoodhealthnetwork.emergencyqr.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import com.thegoodhealthnetwork.emergencyqr.R;

/**
 * Created by kamil on 20.07.15.
 */
public class ComposerDialog extends Dialog {

    private ComposerDialogListener composerDialogListener;

    public ComposerDialogListener getComposerDialogListener() {
        return composerDialogListener;
    }

    public void setComposerDialogListener(ComposerDialogListener composerDialogListener) {
        this.composerDialogListener = composerDialogListener;
    }

    public interface ComposerDialogListener {
        public void saveToPhotosClick();
        public void sendByEmailClick();
        public void previewClick();
        public void onCancelClick();
    }

    public ComposerDialog(Context context) {
        super(context);

        setCanceledOnTouchOutside(false);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.composer_actions_view);

        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        FrameLayout saveToPhotosButton = (FrameLayout)findViewById(R.id.saveToPhotosButton);
        saveToPhotosButton.setOnClickListener(new SaveToPhotosButtonButtonCLickListener());

        FrameLayout sendViaEmailButton = (FrameLayout)findViewById(R.id.sendViaEmailButton);
        sendViaEmailButton.setOnClickListener(new SendViaEmailButtonCLickListener());

        FrameLayout previewButton = (FrameLayout)findViewById(R.id.previewButton);
        previewButton.setOnClickListener(new PreviewButtonCLickListener());

        FrameLayout cancelButton = (FrameLayout)findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new CancelButtonCLickListener());
    }

    private class SaveToPhotosButtonButtonCLickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if(composerDialogListener!=null) {
                composerDialogListener.saveToPhotosClick();
                ComposerDialog.this.cancel();
            }
        }
    }

    private class SendViaEmailButtonCLickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if(composerDialogListener!=null) {
                composerDialogListener.sendByEmailClick();
                ComposerDialog.this.cancel();
            }
        }
    }


    private class PreviewButtonCLickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if(composerDialogListener!=null) {
                composerDialogListener.previewClick();
                ComposerDialog.this.cancel();
            }
        }
    }


    private class CancelButtonCLickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            ComposerDialog.this.cancel();
        }
    }
}
