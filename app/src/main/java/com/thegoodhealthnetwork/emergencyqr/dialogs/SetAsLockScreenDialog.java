package com.thegoodhealthnetwork.emergencyqr.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.thegoodhealthnetwork.emergencyqr.R;

public class SetAsLockScreenDialog extends Dialog {
	private TextView infoViewMessage;
	private TextView titletView;

	public SetAsLockScreenDialog(Context context) {
		super(context);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.setaslockscreen_view);

		setCanceledOnTouchOutside(false);
		setCancelable(false);

		getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

		FrameLayout setLockscreenButton = (FrameLayout)findViewById(R.id.setLockscreenButton);
		setLockscreenButton.setOnClickListener(new SetLoockScreenButtonLickListener());

		FrameLayout shareAppButton = (FrameLayout)findViewById(R.id.shareAppButton);
		shareAppButton.setOnClickListener(new ShareAppButtonLickListener());

		FrameLayout closeButton = (FrameLayout)findViewById(R.id.closeButton);
		closeButton.setOnClickListener(new CloseButtonCLickListener());
	}

	public interface SetAsLockScreenDialogListener {

		public void onSetLoockScreenClick();
		public void onShareAppClick();
	}

	private class CloseButtonCLickListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {

			SetAsLockScreenDialog.this.cancel();
		}
	}

	private class SetLoockScreenButtonLickListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			
			if(setAsLockScreenDialogListener!=null) {
				setAsLockScreenDialogListener.onSetLoockScreenClick();
			}
			
			SetAsLockScreenDialog.this.cancel();
		}
	}

	private class ShareAppButtonLickListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {

			if(setAsLockScreenDialogListener!=null) {
				setAsLockScreenDialogListener.onShareAppClick();
			}

			SetAsLockScreenDialog.this.cancel();
		}
	}
	
	
	
	private SetAsLockScreenDialogListener setAsLockScreenDialogListener;

	public SetAsLockScreenDialogListener getSetAsLockScreenDialogListener() {
		return setAsLockScreenDialogListener;
	}

	public void setSetAsLockScreenDialogListener(SetAsLockScreenDialogListener setAsLockScreenDialogListener) {
		this.setAsLockScreenDialogListener = setAsLockScreenDialogListener;
	}
}
