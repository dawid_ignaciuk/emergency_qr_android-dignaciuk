package com.thegoodhealthnetwork.emergencyqr.dialogs;

import com.thegoodhealthnetwork.emergencyqr.R;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

public class PasswordEmailResetDialog extends Dialog {

	public PasswordEmailResetDialog(Context context,String resetEmail) {
		super(context);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.password_email_reset_view);

		this.setCancelable(false);

		getWindow().setLayout(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT);
		
		final EditText resetCodeText = (EditText)findViewById(R.id.resetCodeText);
		
		TextView resetEmailLable = (TextView)findViewById(R.id.emailLabel);
		resetEmailLable.setText(resetEmail);
		
		FrameLayout okButton = (FrameLayout)findViewById(R.id.okButton);
		okButton.setOnClickListener(new android.view.View.OnClickListener(){

			@Override
			public void onClick(View arg0) {
				if(passwordEmailResetDialogListener!=null) {
					passwordEmailResetDialogListener.onOk(resetCodeText.getText().toString());
				}
				PasswordEmailResetDialog.this.cancel();
			}});
	}
	
	
	public interface PasswordEmailResetDialogListener {
		public void onOk(String codeFromEmail);
	}
	
	private PasswordEmailResetDialogListener passwordEmailResetDialogListener;

	public void setPasswordEmailResetDialogListener(
			PasswordEmailResetDialogListener passwordEmailResetDialogListener) {
		this.passwordEmailResetDialogListener = passwordEmailResetDialogListener;
	}
}