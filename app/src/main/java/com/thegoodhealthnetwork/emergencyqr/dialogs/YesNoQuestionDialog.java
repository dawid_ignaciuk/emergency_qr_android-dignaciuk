package com.thegoodhealthnetwork.emergencyqr.dialogs;

import com.thegoodhealthnetwork.emergencyqr.R;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.TextView;

public class YesNoQuestionDialog extends Dialog {
	private YesNoQuestionDialogListener yesNoQuestionDialogListener;
	
	 public interface YesNoQuestionDialogListener {
	        public void onYesClick();
	        public void onNoClick();
	}
	
	public YesNoQuestionDialog(Context context,String title,String message) {
		super(context);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.yesnoquestion_view);

		TextView titletView = (TextView)findViewById(R.id.titletView);
		titletView.setText(title);

		TextView textView = (TextView)findViewById(R.id.textView);
		textView.setText(message);
	
		getWindow().setLayout(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
		
		FrameLayout yesButton = (FrameLayout)findViewById(R.id.yesButton);
		yesButton.setOnClickListener(new YesButtonCLickListener());
		
		FrameLayout noButton = (FrameLayout)findViewById(R.id.noButton);
		noButton.setOnClickListener(new NoButtonCLickListener());
	}


	public void setYesButtonText(String text) {
		TextView v = (TextView)findViewById(R.id.yesButtonText);
		v.setText(text);
	}

	public void setNoButtonText(String text) {
		TextView v = (TextView)findViewById(R.id.noButtonText);
		v.setText(text);
	}
	
	private class YesButtonCLickListener implements android.view.View.OnClickListener {

		@Override
		public void onClick(View v) {
			if(yesNoQuestionDialogListener!=null) {
				yesNoQuestionDialogListener.onYesClick();
				YesNoQuestionDialog.this.cancel();
			}
		}
	}
	
	private class NoButtonCLickListener implements android.view.View.OnClickListener {

		@Override
		public void onClick(View v) {
			if(yesNoQuestionDialogListener!=null) {
				yesNoQuestionDialogListener.onNoClick();
				YesNoQuestionDialog.this.cancel();
			}
		}
	}

	public YesNoQuestionDialogListener getYesNoQuestionDialogListener() {
		return yesNoQuestionDialogListener;
	}

	public void setYesNoQuestionDialogListener(
			YesNoQuestionDialogListener yesNoQuestionDialogListener) {
		this.yesNoQuestionDialogListener = yesNoQuestionDialogListener;
	}
}
