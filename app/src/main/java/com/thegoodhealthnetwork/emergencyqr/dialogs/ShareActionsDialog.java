package com.thegoodhealthnetwork.emergencyqr.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.FrameLayout;
import com.thegoodhealthnetwork.emergencyqr.R;

public class ShareActionsDialog extends Dialog {
	private ShareActionsDialogListener shareActionsDialogListener;
	
	 public interface ShareActionsDialogListener {
	        public void onSendClick();
	        public void onPrintClick();
	        public void onPostFacebooClick();
	        public void onPostTwitterClick();
	        public void onSendWhatsAppClick();
	        public void onSendMessageWhatsAppClick();
	   
	}
	
	public ShareActionsDialog(Context context) {
		super(context);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.share_actions_view);
	
		getWindow().setLayout(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
		
		FrameLayout sendButton = (FrameLayout)findViewById(R.id.sendButton);
		sendButton.setOnClickListener(new SendButtonCLickListener());
	
		FrameLayout shareFacebookButton = (FrameLayout)findViewById(R.id.shareFacebookButton);
		shareFacebookButton.setOnClickListener(new ShareFacebookButtonCLickListener());
		
		FrameLayout shareTwitterButton = (FrameLayout)findViewById(R.id.shareTwitterButton);
		shareTwitterButton.setOnClickListener(new ShareTwitterButtonCLickListener());
		
		FrameLayout whatsappButton = (FrameLayout)findViewById(R.id.whatsappButton);
		whatsappButton.setOnClickListener(new WhatsappButtonCLickListener());

		FrameLayout cancelButton = (FrameLayout)findViewById(R.id.cancelButton);
		cancelButton.setOnClickListener(new CancelButtonCLickListener());
	
	}
	
	
	private class SendButtonCLickListener implements android.view.View.OnClickListener {

		@Override
		public void onClick(View v) {
			if(shareActionsDialogListener!=null) {
				shareActionsDialogListener.onSendClick();
				ShareActionsDialog.this.cancel();
			}
		}
	}

	private class ShareFacebookButtonCLickListener implements android.view.View.OnClickListener {

		@Override
		public void onClick(View v) {
			if(shareActionsDialogListener!=null) {
				shareActionsDialogListener.onPostFacebooClick();
				ShareActionsDialog.this.cancel();
			}
		}
	}
	
	private class ShareTwitterButtonCLickListener implements android.view.View.OnClickListener {

		@Override
		public void onClick(View v) {
			if(shareActionsDialogListener!=null) {
				shareActionsDialogListener.onPostTwitterClick();
				ShareActionsDialog.this.cancel();
			}
		}
	}
	
	private class WhatsappButtonCLickListener implements android.view.View.OnClickListener {

		@Override
		public void onClick(View v) {
			if(shareActionsDialogListener!=null) {
				shareActionsDialogListener.onSendWhatsAppClick();
				ShareActionsDialog.this.cancel();
			}
		}
	}
	
	private class WhatsappMessageButtonCLickListener implements android.view.View.OnClickListener {

		@Override
		public void onClick(View v) {
			if(shareActionsDialogListener!=null) {
				shareActionsDialogListener.onSendMessageWhatsAppClick();
				ShareActionsDialog.this.cancel();
			}
		}
	}
	
	private class CancelButtonCLickListener implements android.view.View.OnClickListener {

		@Override
		public void onClick(View v) {
			ShareActionsDialog.this.cancel();
		}
	}

	public void setShareActionsDialogListener(
			ShareActionsDialogListener shareActionsDialogListener) {
		this.shareActionsDialogListener = shareActionsDialogListener;
	}
	
	
}
