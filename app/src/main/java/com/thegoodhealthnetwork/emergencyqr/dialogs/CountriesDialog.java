package com.thegoodhealthnetwork.emergencyqr.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ListView;
import com.thegoodhealthnetwork.emergencyqr.R;
import com.thegoodhealthnetwork.emergencyqr.adapters.CountryAdapter;
import com.thegoodhealthnetwork.emergencyqr.global.Country;

public class CountriesDialog extends Dialog {
	private CountriesDialogListener countriesDialogListener;
	private ListView countriesList;
	private CountryAdapter countryAdapter;
	private String selectedCountry;
	
	 public interface CountriesDialogListener {
	        public void onOkClick(String selectedCountry);
	}
	 
	
	public void setCountriesDialogListener(
			CountriesDialogListener countriesDialogListener) {
		this.countriesDialogListener = countriesDialogListener;
	}


	public CountriesDialog(Context context,String countryName) {
		super(context);

		setCanceledOnTouchOutside(false);
		
		selectedCountry=countryName;
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.countries_view);
	
		getWindow().setLayout(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
		
		FrameLayout okButton = (FrameLayout)findViewById(R.id.okButton);
		okButton.setOnClickListener(new OkButtonCLickListener());	
		
		countriesList = (ListView)findViewById(R.id.countriesList);
		prepareCountriesList(context);
	}
	
	
	private void prepareCountriesList(Context context) {
		countryAdapter = new CountryAdapter(context,Country.getCountries(context));
		countriesList.setAdapter(countryAdapter);
		countriesList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		
		if(selectedCountry!=null && !selectedCountry.isEmpty()) {
			
			 countriesList.setSelection(countryAdapter.getSelectCountryPosition(selectedCountry));
			 countriesList.setSelected(true);
			// countriesList.smoothScrollToPosition(countryAdapter.getSelectCountryPosition(selectedCountry));
		}
	}
	
	
	private class OkButtonCLickListener implements android.view.View.OnClickListener {

		@Override
		public void onClick(View v) {
			if(countriesDialogListener!=null) {
				
				countriesDialogListener.onOkClick(countryAdapter.getSelectedCountry());
				
				CountriesDialog.this.cancel();
			}
		}
	}
}
