package com.thegoodhealthnetwork.emergencyqr.dialogs;

import com.thegoodhealthnetwork.emergencyqr.R;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.TextView;

public class InfoDialog extends Dialog {
	private TextView infoViewMessage;
	
	public InfoDialog(Context context) {
		super(context);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.info_view);

		setCanceledOnTouchOutside(false);
	
		getWindow().setLayout(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
		
		FrameLayout okButton = (FrameLayout)findViewById(R.id.okButton);
		okButton.setOnClickListener(new OkButtonCLickListener());
		
		infoViewMessage = (TextView)findViewById(R.id.infoViewMessage);
		
	}
	
	
	private class OkButtonCLickListener implements android.view.View.OnClickListener {

		@Override
		public void onClick(View v) {
			InfoDialog.this.cancel();
		}
	}
	
	public void setInfoText(String text) {
 		infoViewMessage.setText(text);
	}
}
