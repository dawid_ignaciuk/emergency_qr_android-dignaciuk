package com.thegoodhealthnetwork.emergencyqr.dialogs;

import com.thegoodhealthnetwork.emergencyqr.R;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

public class PasswordQuestionDialog extends Dialog {
	private EditText answerText;
	
	public PasswordQuestionDialog(Context context,String question) {
		super(context);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.password_question_view);
		
		this.setCancelable(false);
	
		getWindow().setLayout(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
		
		TextView questionLabel = (TextView)findViewById(R.id.questionLabel);
		questionLabel.setText(question);
		
		answerText = (EditText)findViewById(R.id.answerText);
		
		FrameLayout okButton = (FrameLayout)findViewById(R.id.okButton);
		okButton.setOnClickListener(new OKButtonCLickListener());
		
		
	}
	
	
	private class OKButtonCLickListener implements android.view.View.OnClickListener {

		@Override
		public void onClick(View v) {
			if(passwordQuestionListener!=null) {
				passwordQuestionListener.onAnswer(answerText.getEditableText().toString());
			}
			
			PasswordQuestionDialog.this.cancel();
		}
	}
	
	public interface PasswordQuestionListener { 
		public void onAnswer(String answer);
	}
	
	private PasswordQuestionListener passwordQuestionListener;

	public void setPasswordQuestionListener(
			PasswordQuestionListener passwordQuestionListener) {
		this.passwordQuestionListener = passwordQuestionListener;
	}
	
	
}
