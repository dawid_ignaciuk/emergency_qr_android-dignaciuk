package com.thegoodhealthnetwork.emergencyqr.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;
import com.thegoodhealthnetwork.emergencyqr.R;
import com.thegoodhealthnetwork.emergencyqr.dao.SettingsDataDao;
import com.thegoodhealthnetwork.emergencyqr.model.SettingsModel;

public class SettingsDialog extends Dialog {
	private Activity parent;
	private EditText passwordEdit;
	private EditText repeatPasswordEdit;
	private EditText securityQuestionEdit;
	private EditText securityAnswerEdit;
	private EditText resetEmailEdit;
	private CheckBox disablePAsswordCheckBox;

	public interface  SettingsDialogListener {
		public void onClose();
	}

	private SettingsDialogListener settingsDialogListener;



	public SettingsDialog(Context context, boolean showCancelButton) {
		super(context);
		this.parent = (Activity) context;

		this.setCancelable(false);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.settings_view);

		getWindow().setLayout(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT);

		FrameLayout cancelButton = (FrameLayout) findViewById(R.id.cancelButton);
		cancelButton.setOnClickListener(new CancelButtonCLickListener());

		if (!showCancelButton) {
			cancelButton.setVisibility(View.GONE);
		}

		FrameLayout saveButton = (FrameLayout) findViewById(R.id.saveButton);
		saveButton.setOnClickListener(new SaveButtonCLickListener());

		passwordEdit = (EditText) findViewById(R.id.passwordEdit);
		repeatPasswordEdit = (EditText) findViewById(R.id.repeatPasswordEdit);
		securityQuestionEdit = (EditText) findViewById(R.id.securityQuestionEdit);
		securityAnswerEdit = (EditText) findViewById(R.id.securityAnswerEdit);
		resetEmailEdit = (EditText) findViewById(R.id.resetEmailEdit);

		disablePAsswordCheckBox = (CheckBox) findViewById(R.id.disablePAsswordCheckBox);
		disablePAsswordCheckBox.setOnCheckedChangeListener(new DisablePasswordCheckBoxOnCheckedChangeListener());
		disablePAsswordCheckBox.setChecked(true);

		loadSettings();
	}
	
	private class DisablePasswordCheckBoxOnCheckedChangeListener implements OnCheckedChangeListener {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			enableFields(isChecked);
		}
		
	}

	private void saveSettings(boolean disablePassword) {
		SettingsModel settingsModel = new SettingsModel();

		if (!disablePassword) {
			settingsModel.setPassword(passwordEdit.getText().toString());
			settingsModel.setRepeatePassword(repeatPasswordEdit.getText()
					.toString());
			settingsModel.setPasswordResetQuestion(securityQuestionEdit
					.getText().toString());
			settingsModel.setPasswordResetAnswer(securityAnswerEdit.getText()
					.toString());
			settingsModel.setPasswordResetEmail(resetEmailEdit.getText()
					.toString());
			settingsModel.setPasswordDisabled(false);
		} else {
			settingsModel.setPassword(null);
			settingsModel.setRepeatePassword(null);
			settingsModel.setPasswordResetQuestion(null);
			settingsModel.setPasswordResetAnswer(null);
			settingsModel.setPasswordResetEmail(null);
			;
			settingsModel.setPasswordDisabled(true);
		}

		SettingsDataDao settingsDataDao = new SettingsDataDao();
		try {
			settingsDataDao.save(settingsModel, !disablePassword);
			SettingsDialog.this.cancel();
			Toast.makeText(parent, "Settings saved", Toast.LENGTH_LONG).show();

		} catch (Exception e) {
			ErrorDialog errorDialog = new ErrorDialog(
					SettingsDialog.this.parent, "Error. Can't save settings. "
							+ e.getMessage());
			errorDialog.show();
		}
	}

	private void loadSettings() {
		SettingsDataDao settingsDataDao = new SettingsDataDao();

		try {
			SettingsModel settingsModel = settingsDataDao.load();

			if (settingsModel != null) {
				disablePAsswordCheckBox.setChecked(!settingsModel
						.isPasswordDisabled());

				if (!settingsModel.isPasswordDisabled()) {
					securityQuestionEdit.setText(settingsModel
							.getPasswordResetQuestion());
					securityAnswerEdit.setText(settingsModel
							.getPasswordResetAnswer());
					resetEmailEdit.setText(settingsModel
							.getPasswordResetEmail());
				} else {
					enableFields(false);
				}
			}

		} catch (Exception e) {
			SettingsDialog.this.cancel();
			Toast.makeText(parent,
					"Error. Can't load settings " + e.getMessage(),
					Toast.LENGTH_LONG).show();
		}
	}
	
	private void enableFields(boolean endable) {
		securityQuestionEdit.setEnabled(endable);
		securityAnswerEdit.setEnabled(endable);
		resetEmailEdit.setEnabled(endable);
		passwordEdit.setEnabled(endable);
		repeatPasswordEdit.setEnabled(endable);
	}

	private class CancelButtonCLickListener implements
			android.view.View.OnClickListener {

		@Override
		public void onClick(View v) {
			if(!disablePAsswordCheckBox.isChecked()) {
				saveSettings(true);
			}

			SettingsDialog.this.cancel();
		}
	}

	private class SaveButtonCLickListener implements
			android.view.View.OnClickListener {

		@Override
		public void onClick(View v) {
			if (!disablePAsswordCheckBox.isChecked()) {
				saveSettings(true);
			} else {
				saveSettings(false);
			}
		}
	}

	@Override
	public void onBackPressed() {
		parent.onBackPressed();
	}
}
