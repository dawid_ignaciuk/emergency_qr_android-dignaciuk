package com.thegoodhealthnetwork.emergencyqr.dialogs;

import com.thegoodhealthnetwork.emergencyqr.R;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.TextView;

public class TextViewDialog extends Dialog {
		
	public TextViewDialog(Context context,String title,String text) {
		super(context);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.textview_view);
	
		getWindow().setLayout(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
		
		TextView titletView = (TextView)findViewById(R.id.titletView);
		titletView.setText(title);
		
		FrameLayout okButton = (FrameLayout)findViewById(R.id.okButton);
		okButton.setOnClickListener(new OKButtonCLickListener());
		
		TextView valueTextView = (TextView)findViewById(R.id.valueTextView);
		valueTextView.setText(text);
	}
	
	
	private class OKButtonCLickListener implements android.view.View.OnClickListener {

		@Override
		public void onClick(View v) {
			
			TextViewDialog.this.cancel();
		}
	}
}
